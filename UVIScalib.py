import path_register
pr = path_register.PathRegister()
gitpath = pr.gitpath

from datetime import datetime, timedelta
import glob
import numpy as np
import os
import scipy.interpolate
import scipy.io as sio
import time_conversions as tc

# path to the calibration data files
calib_path = '{}/uvis_stitch/CALIB_DATA/'.format(gitpath)

channel = 'FUV'

ettime0 = tc.datetime2et(datetime(1990,1,15,3,15))
ettime1 = tc.datetime2et(datetime(2013,1,15,3,15))
ettime2 = tc.datetime2et(datetime(2016,1,15,3,15))


def get_uvis_lab_sensitivity(channel):
    if channel == 'EUV':
        print('No EUV lab sensitivity available')
        return
    elif channel == 'FUV':
        lsfile = '%s/FUV_1999_Lab_Cal.dat' % calib_path
        lsdata = np.genfromtxt(lsfile, skip_header=1)
        wcal = np.append(lsdata[:,0], lsdata[:,3])
        ucal = np.append(lsdata[:,1], lsdata[:,4])
        ucalerr = np.append(lsdata[:,2], lsdata[:,5])
        return wcal, ucal, ucalerr
    
def f_flight_wavelength(bbin=1):
    rad2deg = 180/np.pi
    d = 1e7/1066
    alpha = (9.22+0.032)/rad2deg
    alpha += 3.46465e-5       # 11-1-99 Adjustment
    beta = (np.arange(1024)-511.5)*0.025*0.99815/300
    beta = np.arctan(beta)+0.032/rad2deg+3.46465e-5       # 11-1-99 Adjustment
    lam = d*(np.sin(alpha)+np.sin(beta))
    if bbin == 1:
        e_wavelength = lam
        return e_wavelength
    else:
        e_wavelength = np.full((int(1024/bbin)), np.nan)
        for iii in range(len(e_wavelength)):
            e_wavelength[iii] = np.mean(lam[iii*bbin:(iii+1)*bbin])
        return e_wavelength
    
def get_uvis_cal_modifier(ettime):
    spicafile = '%s/spica_variability4_data.sav' % calib_path
    spicadata = sio.readsav(spicafile)
    # get time parameters
    year = np.array([np.asscalar(spicadata['arr'].desc[iii].year_start) for iii in range(len(spicadata['arr']))])
    doy = np.array([np.asscalar(spicadata['arr'].desc[iii].doy_start) for iii in range(len(spicadata['arr']))])
    hour = np.array([np.asscalar(spicadata['arr'].desc[iii].hour_start) for iii in range(len(spicadata['arr']))])
    minute = np.array([np.asscalar(spicadata['arr'].desc[iii].min_start) for iii in range(len(spicadata['arr']))])
    second = np.array([np.asscalar(spicadata['arr'].desc[iii].sec_start) for iii in range(len(spicadata['arr']))])
    # convert to datetime
    spicatimes = np.array([datetime(year=year[iii],month=1,day=1,
                                    hour=hour[iii],minute=minute[iii],
                                    second=int(np.floor(second[iii])))
                                            +timedelta(days=int(doy[iii]-1))
                                            for iii in range(len(spicadata['arr']))])
    etspicatimes = tc.datetime2et(spicatimes)
    # if observation was before the first file, return array of 1s
    if ettime < etspicatimes[0]:
        return np.ones((1024))
    # if observation was after the last file, return last array
    elif ettime > etspicatimes[-1]:
        specmod = spicadata['arr'].ratio[-1]
        return specmod
    # if observation was in between two files, do linear interpolation
    else:
        arg1 = np.squeeze(np.where(etspicatimes<ettime))[-1]
        arg2 = np.squeeze(np.where(etspicatimes>ettime))[0]
        specmod1 = spicadata['arr'].ratio[arg1]
        specmod2 = spicadata['arr'].ratio[arg2]
        m = (specmod2-specmod1)/(etspicatimes[arg2]-etspicatimes[arg1])
        specmod = specmod1+m*(ettime-etspicatimes[arg1])
        return specmod
    
def read_spica_ff_data(ettime):
    ettime_starburn = tc.datetime2et(datetime(year=2002, month=6,day=6))
    if ettime >= ettime_starburn:
        file = '%s/FLATFIELD_FUV_POSTBURN.txt' % calib_path
    else:
        file = '%s/FLATFIELD_FUV_PREBURN.txt' % calib_path
    ff = np.zeros((64,1024))
    # header lines to skip
    skip_header_zero = 3
    # read in block by block
    for iii in range(64):
        thisskip = skip_header_zero+iii*173
        tmp1 = np.genfromtxt(file, skip_header=thisskip, max_rows=170)
        tmp2 = np.genfromtxt(file, skip_header=thisskip+170, max_rows=1)
        tmp = np.append(np.ravel(tmp1), tmp2)
        ff[iii,:] = tmp
    return ff

def get_uvis_ff_modifier(ettime, channel):
    # get times of ff modifier files
    ffpath = '%s/UVIS_flat-field_modifiers_2016-01-13' % calib_path
    fffiles = glob.glob('%s/%s20*.dat' % (ffpath, channel))
    fftimes = [fffiles[iii].split('_UVIS')[0] for iii in range(len(fffiles))]
    fftimes = [fftimes[iii].split(channel)[-1] for iii in range(len(fftimes))]
    fftimes = [datetime.strptime(fftimes[iii], '%Y_%j_%H_%M_%S') for iii in range(len(fftimes))]
    fftimes = tc.datetime2et(fftimes)
    # if observation was before the first ff modifier file, return array of 1s
    if ettime < fftimes[0]:
        return np.ones((64,1024))
    # if observation was after the last ff modifier file, return last array
    elif ettime > fftimes[-1]:
        ffarray = np.squeeze(np.fromfile(fffiles[-1], dtype='<(64,1024)f4', count=-1))
        ffarray[61,:] = 1 # WHY?
        return ffarray
    # if observation was in between two ff modifier files, do linear interpolation
    else:
        arg1 = np.where(fftimes<ettime)[0][-1]
        arg2 = np.where(fftimes>ettime)[0][0]
        ffarray1 = np.squeeze(np.fromfile(fffiles[arg1], dtype='<(64,1024)f4', count=-1))
        ffarray2 = np.squeeze(np.fromfile(fffiles[arg2], dtype='<(64,1024)f4', count=-1))
        m = (ffarray2-ffarray1)/(fftimes[arg2]-fftimes[arg1])
        ffarray = ffarray1+m*(ettime-fftimes[arg1])
        ffarray[61,:] = 1 # WHY?
        return ffarray

# interpolate each spectrum
# input in [line,band]
def interpolate_nans2(wave, arrin):
    arrout = np.zeros(np.shape(arrin))
    for iii in range(np.shape(arrin)[0]):
        spec = np.copy(arrin[iii,:])
        # no nan values
        if not np.sum(np.isnan(spec)):
            arrout[iii,:] = spec
        elif len(spec)-np.sum(np.isnan(spec)) <=1:
            print('Warning: only nan values, spectrum could not be interpolated')
            arrout[iii,:] = spec
        else:
            # interpolate
            ndxfin = np.where(np.isfinite(spec))
            ndxnan = np.where(np.isnan(spec))
            f = scipy.interpolate.interp1d(wave[ndxfin], spec[ndxfin], fill_value='extrapolate')
            spec[ndxnan] = f(wave[ndxnan])
            arrout[iii,:] = spec
    return arrout
    
            
# window_def = [UL_BAND, UL_LINE, LR_BAND, LR_LINE]
# bin_def = [BAND_BIN, LINE_BIN]
def get_uvis_2015_calibration_interp(ettime, channel, swidth, window_def, bin_def):
    if channel not in ['EUV', 'FUV']:
        print('Invalid channel')
        return
    if channel == 'EUV':
        print('EUV calibration not implemented.')
        return
    if swidth<1 or swidth>2:
        print('Invalid slit width')
        return
    #-----
    # get lab sensitivities
    wcal, ucal, ucalerr = get_uvis_lab_sensitivity(channel)
    #-----
    # apply slit width correction
    ucal /= swidth
    ucalerr /= swidth
    #-----
    # get wavelength scale
    wf = f_flight_wavelength()
    # interpolate the discrete-wavelength sensitivity to the
    # full spectral range of the detector
    intfunc = scipy.interpolate.interp1d(wcal, ucal, fill_value='extrapolate')
    ucal = intfunc(wf)/60
    intfunc = scipy.interpolate.interp1d(wcal, ucalerr, fill_value='extrapolate')
    ucalerr = intfunc(wf)/60
    #-----
    # define the average pixel bandpass in [A] (dispersion x pixel width)
    pixel_bandpass = 0.78
    # apply factor for continuous input spectrum
    # sensitivity units now (c/s) /kR/A
    ucal *= pixel_bandpass
    ucalerr *= pixel_bandpass
    #-----
    # apply a time-dependent adjustment to the lab-measured sensitivity based on
    # repeated observations of Spica
    specmod = get_uvis_cal_modifier(ettime)
    ucal *= specmod
    ucalerr *= specmod              # not in the original code but should be?
    # construct a 2D array and fill with the 1D sensitivity
    u_cal = np.zeros((64,1024))
    u_cal_err = np.zeros_like(u_cal)
    for kkk in range(2,62):
        u_cal[kkk,:] = ucal
        u_cal_err[kkk,:] = ucalerr
    #-----
    # apply flat field correction
    # adjust sensitivity to account for elimination of evil pixels in original calibration
    ucal /= 0.91
    ucalerr /= 0.91
    ff = read_spica_ff_data(ettime)
    ndxnan = np.where(np.isnan(ff))
    # adjust flat field normalization to account for asymmetry in histogram distribution
    ff *= 1.05
    # row 2 and row 61 in the FUV flat-field corrector appear erroneous
    # for now eliminate the corrector by setting to 1
    ff[2,:] = 1
    ff[61,:] = 1
    # apply the flat field corrector
    # the flat field must be multiplied by the data, or equivalently
    # divided into the _sensitivity_ (calibration=1/sensitivity)
    u_cal /= ff
    u_cal_err /= ff
    # apply the flat field modifier
    arrmod = get_uvis_ff_modifier(ettime, channel)
    arrmod[ndxnan] = 1
    u_cal *= arrmod
    u_cal_err *= arrmod
    
    
    #-----
    # ADDED ABA
    # interpolate the correction before collapsing
    u_cal = interpolate_nans2(wf,u_cal)
    
    #-----
    # get sizing and binning
    # make to int
    ul_band, ul_line, lr_band, lr_line = np.array(np.squeeze(window_def)).astype(int)
    band_bin, line_bin = np.array(np.squeeze(bin_def)).astype(int)
    # size of unbinned window
    band_size = int(lr_band-ul_band+1)
    line_size = int(lr_line-ul_line+1)
    # pad to integral multiples of the binning parameters
    band_size_pad = int(band_bin*np.ceil(band_size/band_bin))
    line_size_pad = int(line_bin*np.ceil(line_size/line_bin))
    # size of final returned array (partial bins at the end included)
    band_size_final = int(np.ceil(band_size/band_bin))
    line_size_final = int(np.ceil(line_size/line_bin))
    
    #-----
    # make binned and padded versions of wavelength vector, sensitivity,
    # and sensitivity uncertainty
    wave_temp = np.zeros((band_size_pad))
    u_cal_temp = np.zeros((line_size_pad, band_size_pad))
    u_cal_err_temp = np.zeros((line_size_pad, band_size_pad))
    wave_temp[:band_size] = wf[ul_band:(lr_band+1)]
    u_cal_temp[:line_size,:band_size] = u_cal[ul_line:(lr_line+1), ul_band:(lr_band+1)]
    u_cal_err_temp[:line_size,:band_size] = u_cal_err[ul_line:(lr_line+1), ul_band:(lr_band+1)]
    
    #-----
    # make final versions
    u_wavelength = np.zeros((band_size_final))
    cal_temp = np.zeros((line_size_final, band_size_final))
    err_temp = np.zeros((line_size_final, band_size_final))
    # average wavelength values within each bin
    for bbb in range(band_size_final):
        u_wavelength[bbb] = np.nanmean(wave_temp[bbb*band_bin:(bbb+1)*band_bin])
    
    for lll in range(line_size_final):
        for bbb in range(band_size_final):
            # sum the sensitivity
            cal_temp[lll,bbb] = np.sum(
                    u_cal_temp[lll*line_bin:(lll+1)*line_bin,
                               bbb*band_bin:(bbb+1)*band_bin])
            # sum the sensitivity uncertainty in quadrature
            err_temp[lll,bbb] = np.sum(
                    u_cal_err_temp[lll*line_bin:(lll+1)*line_bin,
                                   bbb*band_bin:(bbb+1)*band_bin])/np.sqrt(band_bin*line_bin)
    cal_temp[np.where(cal_temp == 0)] = 1e10
    err_temp[np.where(err_temp == 0)] = 1e10
    #-----
    # invert the sensitivity to obtain the calibration factor
    # units will be in (kR/A) / (counts/sec)
    u_calibration = 1/cal_temp
    u_calibration_error = err_temp/cal_temp**2
    u_calibration[np.where(u_calibration<1e-9)] = 0
    u_calibration_error[np.where(u_calibration_error<1e-9)] = 0
           
    #-----
    # return nan-interpolated values
    return u_wavelength, u_calibration, u_calibration_error
#    return u_wavelength, interpolate_nans2(u_wavelength,u_calibration), u_calibration_error
    