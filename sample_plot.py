import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
import get_image
import glob
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.patches as ptch
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import stitch_calibrate_project
import time_conversions as tc

us = stitch_calibrate_project.UVISstitcher()

myblue = 'royalblue'
myred = 'crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)

plotpath = '{}sample_data'.format(__file__.strip('sample_plot.py'))
if not os.path.exists(plotpath):
    os.makedirs(plotpath)
    
datafile = glob.glob('{}/*.dat'.format(plotpath))[0]
datafile = datafile.split('.')[0]

us.setSavePath(plotpath)
us.setResolution(2)
us.saveSpecs(False)
try:
    projfile = '{}/2016_278T09_22_40.fits'.format(plotpath)
    us.getCalibIntegData(datafile, LASP=False)
except:
    us.stitchFiles(datafile, LASP=False)
image, angles, _ = get_image.getRedUVIS(projfile)
    
unproj = us.int_data
ettimes = us.ettimes
dttimes = tc.et2datetime(ettimes)
start, stop = us.getSplitLimits(datafile)
start = np.delete(start, [1,3])
start[0] = start[0]+3
stop = np.delete(stop, [0,2])

fig = plt.figure()
fig.set_size_inches(15,15)

gs = gridspec.GridSpec(2,3, width_ratios=(1,1,0.07), hspace=0.1, wspace=0.1)

# =============================================================================
# integrated data
# =============================================================================
ax = plt.subplot(gs[:, 0]) 
data = np.copy(unproj)
data[data<0.1] = 0.1
quad = ax.pcolormesh(np.arange(65), dttimes, data, cmap=cmap_UV)
quad.set_norm(mcolors.LogNorm(0.5,30))
quad.cmap.set_under('k')
quad.cmap.set_over('w')

ax.set_facecolor('grey')
ax.set_ylim([dttimes[-1], dttimes[0]])
ax.yaxis.set_major_locator(mdates.MinuteLocator(byminute=[0,30]))
ax.yaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
ax.yaxis.set_minor_locator(mdates.MinuteLocator(byminute=[0,10,20,30,40,50]))

shift = 3
for iii in range(len(start)):
    rect = ptch.Rectangle((0.5,mdates.date2num(dttimes[int(start[iii]+1)])),
                          63,
                          (mdates.date2num(dttimes[int(stop[iii]-shift)])
                          -mdates.date2num(dttimes[int(start[iii]+shift)])),
                          linewidth=3,linestyle='-',
                          edgecolor='w',facecolor='none',
                          alpha=0.8, zorder=4)
    ax.add_patch(rect)
    rect = ptch.Rectangle((0.5,mdates.date2num(dttimes[int(start[iii]+1)])),
                          63,
                          (mdates.date2num(dttimes[int(stop[iii]-shift)])
                          -mdates.date2num(dttimes[int(start[iii]+shift)])),
                          linewidth=2,linestyle='--',
                          edgecolor=myblue,facecolor='none',
                          alpha=1, zorder=5)
    ax.add_patch(rect)
    
    if iii < len(start)-1:
        rect = ptch.Rectangle((0.5,mdates.date2num(dttimes[int(stop[iii]+1)])),
                              63,
                              (mdates.date2num(dttimes[int(start[iii+1]-shift)])
                              -mdates.date2num(dttimes[int(stop[iii]+shift)])),
                              linewidth=3,linestyle='-',
                              edgecolor='w',facecolor='none',
                              alpha=0.8, zorder=4)
        ax.add_patch(rect)
        rect = ptch.Rectangle((0.5,mdates.date2num(dttimes[int(stop[iii]+1)])),
                              63,
                              (mdates.date2num(dttimes[int(start[iii+1]-shift)])
                              -mdates.date2num(dttimes[int(stop[iii]+shift)])),
                              linewidth=2,linestyle='--',
                              edgecolor=myred,facecolor='none',
                              alpha=1, zorder=5)
        ax.add_patch(rect)
        

ax.set_xlabel('Pixel', fontsize=14)
ticks = np.arange(0,65,7)
ax.set_xticks(ticks+0.5)
ax.set_xticklabels(ticks+1)
ax.set_xlim([0,64])
plt.tick_params(axis='both', which='major', labelsize=12)

txt = ax.text(0.02,0.985,'(a)', ha='left', va='top',
        fontsize=20, fontweight='bold',
        color='w',
        transform=ax.transAxes)
txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])

# =============================================================================
# projected data
# =============================================================================

ax = plt.subplot(gs[0,1], projection='polar')

theta = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1)
r = np.linspace(0,30,num=np.shape(image)[1]+1)

image[np.where(image<=0)] = 0.00001
    
# plot colormap and colorbar
quad = ax.pcolormesh(theta, r, image.T, cmap=cmap_UV)
quad.set_norm(mcolors.LogNorm(0.5,30))
ax.set_facecolor('gray')

subax = plt.subplot(gs[0,2])
cbar = plt.colorbar(quad, cax=subax, extend='both')
cbar.set_label('Intensity (kR)', labelpad=15, fontsize=14, rotation=270)
cbar.set_ticks(np.append(np.append(np.arange(0.1,1,0.1),
                                   np.arange(1,10,1)),
                         np.arange(10,101,10)))
cbar.ax.tick_params(labelsize=12)
        
ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=16, fontweight='bold',
                  ha='center',va='center')
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax.set_xticklabels([])
ax.tick_params(axis='x', which='major', pad=-20)
ax.tick_params(axis='x', which='major', color='w')
ax.set_yticks([10,20,30])
ax.set_yticklabels([])
ax.grid('on', color='0.8', linewidth=1.5)
ax.set_theta_zero_location("N")
ax.set_rmax(30)
ax.set_rmin(0)
#ax.set_title(times[imgctr], fontsize=13, y=1.0)

ax.text(0.02,0.98,'(b)', ha='left', va='top',
        fontsize=20, fontweight='bold',
        transform=ax.transAxes)


# =============================================================================
# elevation angles
# =============================================================================

ax = plt.subplot(gs[1,1], projection='polar') 

# plot colormap and colorbar
quad = ax.pcolormesh(theta, r, angles.T, cmap=cmap_UV, vmin=30, vmax=90)
ax.set_facecolor('gray')

subax = plt.subplot(gs[1,2])
cbar = plt.colorbar(quad, cax=subax, extend='both')
cbar.set_label('Elevation angle (deg)', labelpad=18, fontsize=14, rotation=270)
cbar.set_ticks(np.arange(30,91,10))
cbar.ax.tick_params(labelsize=12)
        
ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=16, fontweight='bold',
                  ha='center',va='center')
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax.set_xticklabels([])
ax.tick_params(axis='x', which='major', pad=-20)
ax.tick_params(axis='x', which='major', color='w')
ax.set_yticks([10,20,30])
ax.set_yticklabels([])
ax.grid('on', color='0.8', linewidth=1.5)
ax.set_theta_zero_location("N")
ax.set_rmax(30)
ax.set_rmin(0)

ax.text(0.02,0.98,'(c)', ha='left', va='top',
        fontsize=20, fontweight='bold',
        transform=ax.transAxes)

plt.savefig('{}/proj_sample_2016_278T09_22_40.png'.format(plotpath),bbox_inches='tight', dpi=100)
plt.savefig('{}/proj_sample_2016_278T09_22_40.pdf'.format(plotpath),bbox_inches='tight')
plt.show() 
plt.close()
