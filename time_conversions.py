import collections
from datetime import datetime
import numpy as np
import spiceypy as spice

lskpath = __file__.split('time_conversions')[0]
lsk = '%s/naif0012.tls' % lskpath

# load lsk if not loaded already
try:
    spice.kinfo(lsk)
except spice.stypes.SpiceyError:
    spice.kclear()
    spice.furnsh(lsk)

# Turns spice/ET time into python datetime
# Input: ET 1-D array/list or single value
def et2datetime(ettimes):
    if not isinstance(ettimes, collections.Iterable):
        ettimes = [ettimes]
    utctimes = np.array([spice.timout(iii, 'YYYY-MM-DD, HR:MN:SC.###') for iii in ettimes])
    pytimes = np.array([datetime.strptime(iii, '%Y-%m-%d, %H:%M:%S.%f') for iii in utctimes])
    if len(pytimes)==1:
        return np.asscalar(pytimes)
    else:
        return pytimes

# Turns python datetime into spice/ET time
# Input: datetime 1-D array/list or single value
def datetime2et(pytimes):
    if not isinstance(pytimes, collections.Iterable):
        pytimes = [pytimes]
    utctimes = np.array([datetime.strftime(iii, '%Y-%m-%d, %H:%M:%S.%f') for iii in pytimes])
    ettimes = np.array([spice.utc2et(iii) for iii in utctimes])
    if len(ettimes)==1:
        return np.asscalar(ettimes)
    else:
        return ettimes

# Turns python datetime into days after 2004-01-01 time
# 2004-01-01 is doy2004 0
def et2doy2004(ettimes):
    ettimes = np.array(ettimes)
    deltasec = ettimes - datetime2et(datetime(2004,1,1,0,0,0))
    doy2004 = deltasec/3600/24
    if np.size(doy2004)==1:
        return np.asscalar(doy2004)
    else:
        return doy2004
    
# Turns days after 2004-01-01 time into python datetime
# 2004-01-01 is doy2004 0
def doy20042et(doy2004):
    doy2004 = np.array(doy2004)
    deltasec = doy2004*3600*24
    ettimes = deltasec + datetime2et(datetime(2004,1,1,0,0,0))
    if np.size(ettimes)==1:
        return np.asscalar(ettimes)
    else:
        return ettimes
    
def datetime2doy2004(pytimes):
    ettimes = datetime2et(pytimes)
    doy2004 = et2doy2004(ettimes)
    return doy2004

def doy20042datetime(doy2004):
    ettimes = doy20042et(doy2004)
    pytimes = et2datetime(ettimes)
    return pytimes