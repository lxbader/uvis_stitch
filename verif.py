import UVIScalib
import scipy.io as sio
from datetime import datetime
import time_conversions as tc
import matplotlib.pyplot as plt
import numpy as np

def makemap(data):
    plt.figure()
    tmp = plt.pcolormesh(data)
    plt.colorbar(tmp)
    plt.show()
    plt.close()

pytime = datetime.strptime('2008-224T04:30:06', '%Y-%jT%H:%M:%S')
ettime = tc.datetime2et(pytime)
swidth=1
channel = 'FUV'
window_def = [0,0,1023,63]
bin_def = [16,1]

u_wave, u_cal, u_err = UVIScalib.get_uvis_2015_calibration_interp(ettime, channel, swidth, window_def, bin_def)

#testfile = 'E:/calib_test/test2008_224T04_30_bin%sx%s.sav' % (bin_def[0], bin_def[1])
#data = sio.readsav(testfile)
#
#makemap(data['cal'])
#test_sarah= UVIScalib.interpolate_nans2(u_wave,data['cal'])
#makemap(test_sarah)

makemap(u_cal)
test_mine = UVIScalib.interpolate_nans2(u_wave,u_cal)
makemap(test_mine)
#test = np.zeros_like(u_cal)
#makemap(test_mine)

#makemap(np.abs(test_sarah-test_mine))

#bin_def = [1,1]
#testfile = 'E:/calib_test/test2008_224T04_30_bin%sx%s_mods.sav' % (bin_def[0], bin_def[1])
#data_mods = sio.readsav(testfile)
#
#arrmod = UVIScalib.get_uvis_ff_modifier(ettime, channel)
#print('arrmod')
#makemap(data_mods['arrmod'])
#makemap(arrmod)
#makemap(np.abs(arrmod-data_mods['arrmod']))