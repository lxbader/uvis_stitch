import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
import os
import spiceypy as spice
import time
import time_conversions as tc
import UVIScalib
import warnings
warnings.simplefilter('ignore')

spice.kclear()
spice.furnsh('%s/SPICE/metakernels/cassini_generic.mk' % datapath)

# import PDS standard file
file = r'D:\PhD\Box Sync\LASP UVIS SAMPLE FILES\PDS\FUV2017_017_06_30'
print(file)
metalabels = ['RECORD_TYPE','FILE_RECORDS','PRODUCT_ID','START_TIME','STOP_TIME',
      'INTEGRATION_DURATION','SLIT_STATE','^QUBE','AXES','AXIS_NAME',
      'CORE_ITEMS','CORE_ITEM_BYTES','CORE_ITEM_TYPE','CORE_BASE',
      'CORE_MULTIPLIER','CORE_UNIT','UL_CORNER_LINE','UL_CORNER_BAND',
      'LR_CORNER_LINE','LR_CORNER_BAND','BAND_BIN','LINE_BIN']
      
metadata = {}  
# IMPORT DATA FILE
# read lbl file
datfile = '%s.dat' % file
lblfile = '%s.lbl' % file
with open(lblfile,'r') as lf:
    line = lf.readline()
    while line:
        for label in metalabels:
            if label in line and '= ' in line:
                value = line.strip('\n')
                value = value.split('= ')[1]
                value = value.strip('"')
                value = value.strip('(')
                value = value.strip(')')
                value = value.strip(') ')
                value = value.strip(' <SECOND>')
                try:
                    metadata[label] = float(value)
                except:
                    metadata[label] = value
        line = lf.readline()

# get size of array
axissize = [int(iii) for iii in metadata['CORE_ITEMS'].split(', ')]
if not len(axissize)==3:
    print('D')
data_fmt = '>(%d,%d,%d)u2' % (axissize[-1], axissize[-2], axissize[-3])
if not axissize[-1] == metadata['FILE_RECORDS']:
    print('E')

print('Expected filesize:')
print(metadata['FILE_RECORDS']*1024*64*2, '= {}*{}*{}*{}'.format(metadata['FILE_RECORDS'],1024,64,2))
print('Actual filesize:')
print(os.path.getsize(datfile))
# read dat file
data = np.squeeze(np.fromfile(datfile, dtype=data_fmt, count=-1))

# get timestamps
tstart = datetime.strptime(metadata['START_TIME'], '%Y-%jT%H:%M:%S.%f')
pytimes = np.array([tstart+iii*timedelta(seconds=metadata['INTEGRATION_DURATION'])
                    for iii in range(int(metadata['FILE_RECORDS'])+1)])
ettimes = tc.datetime2et(pytimes)

# get valid band indices
bbin = int(metadata['BAND_BIN'])
bmin = 0
bmax = int(np.ceil((metadata['LR_CORNER_BAND']-metadata['UL_CORNER_BAND']+1)/bbin))

# get valid line indices
# no line_bin included yet, sorting unclear in case of binning higher than 1
lbin = int(metadata['LINE_BIN'])
lmin = int(metadata['UL_CORNER_LINE'])
lmax = int(metadata['LR_CORNER_LINE'])
# data starts at UL_CORNER, and fills LR_CORNER+1-UL_CORNER following pixels
lmin_val = lmin
numpx = int(np.ceil((lmax+1-lmin)/lbin))
lmax_val = int(lmin_val+numpx)

    
# get slit width
if 'LOW' in metadata['SLIT_STATE']:
    swidth = 1
else:
    swidth = 2

# CALIBRATE
print('%s Calibrating...' % time.strftime('%Y-%m-%d %H:%M:%S'))
# get calibration arrays
window_def = [metadata['UL_CORNER_BAND'], metadata['UL_CORNER_LINE'],
              metadata['LR_CORNER_BAND'], metadata['LR_CORNER_LINE']]
bin_def = [metadata['BAND_BIN'], metadata['LINE_BIN']]
wave, cal, calerr = UVIScalib.get_uvis_2015_calibration_interp(ettimes[0],
                                                               'FUV',
                                                               swidth,
                                                               window_def,
                                                               bin_def)

# perform calibration
valid_data = data[:,lmin_val:lmax_val,bmin:bmax]
valid_data = np.array(valid_data, dtype=np.float32)
#valid_data /= metadata['INTEGRATION_DURATION']
test = np.sum(valid_data, axis=-1)
plt.pcolormesh(np.log10(test.T))
plt.show()


#=========================================================================================================
#=========================================================================================================
# do the same with LASP file
file_LASP = r'D:\PhD\Box Sync\LASP UVIS SAMPLE FILES\FUV2017_017_06_30_10_UVIS_257SA_AURSTARE002_PRIME'

print(file)
metalabels = ['RECORD_TYPE','FILE_RECORDS','PRODUCT_ID','START_TIME','STOP_TIME',
      'INTEGRATION_DURATION','SLIT_STATE','^QUBE','AXES','AXIS_NAME',
      'CORE_ITEMS','CORE_ITEM_BYTES','CORE_ITEM_TYPE','CORE_BASE',
      'CORE_MULTIPLIER','CORE_UNIT','UL_CORNER_LINE','UL_CORNER_BAND',
      'LR_CORNER_LINE','LR_CORNER_BAND','BAND_BIN','LINE_BIN']
metalabels_LASP = [iii.replace('_',' ') for iii in metalabels]
      
metadata_LASP = {}  
# IMPORT DATA FILE
# read lbl file
datfile_LASP = '%s' % file_LASP
lblfile_LASP = '%s.metadata' % file_LASP
with open(lblfile_LASP,'r') as lf:
    line = lf.readline()
    while line:
        for label in metalabels_LASP:
            if label in line and '= ' in line:
                value = line.strip('\n')
                value = value.split('= ')[1]
                value = value.strip('"')
                value = value.strip('(')
                value = value.strip(')')
                value = value.strip(') ')
                value = value.strip(' <SECOND>')
                try:
                    metadata_LASP[label.replace(' ','_')] = float(value)
                except:
                    metadata_LASP[label.replace(' ','_')] = value
        line = lf.readline()

# get size of array
axissize_LASP = [int(iii) for iii in metadata_LASP['CORE_ITEMS'].split(', ')]
if not len(axissize_LASP)==3:
    print('D')
if not axissize_LASP[-1] == metadata_LASP['FILE_RECORDS']:
    print('E')

os.path.getsize(datfile_LASP)
data_fmt_LASP = '>({0:d},{1:d},{2:d})u2'.format(axissize_LASP[-1],
                  int(axissize_LASP[-2]/metadata_LASP['LINE_BIN']),
                  int(axissize_LASP[-3]/metadata_LASP['BAND_BIN']))


print('Expected filesize:')
print(metadata['FILE_RECORDS']*int(axissize_LASP[-3]/metadata_LASP['BAND_BIN'])*int(axissize_LASP[-2]/metadata_LASP['LINE_BIN'])*2,
      '= {}*{}*{}*{}'.format(metadata['FILE_RECORDS'],int(axissize_LASP[-3]/metadata_LASP['BAND_BIN']),int(axissize_LASP[-2]/metadata_LASP['LINE_BIN']),2))
print('Actual filesize:')
print(os.path.getsize(datfile_LASP))
# read dat file
f = open(datfile_LASP, 'rb')
f.seek(800, os.SEEK_SET)
data_LASP = np.squeeze(np.fromfile(f, dtype=data_fmt_LASP, count=-1))


# get timestamps
tstart = datetime.strptime(metadata_LASP['START_TIME'], '%Y-%jT%H:%M:%S.%f')
pytimes = np.array([tstart+iii*timedelta(seconds=metadata_LASP['INTEGRATION_DURATION'])
                    for iii in range(int(metadata_LASP['FILE_RECORDS'])+1)])
ettimes = tc.datetime2et(pytimes)

# get valid band indices
bbin = int(metadata_LASP['BAND_BIN'])
bmin = 0
bmax = int(np.ceil((metadata_LASP['LR_CORNER_BAND']-metadata_LASP['UL_CORNER_BAND']+1)/bbin))

# get valid line indices
# no line_bin included yet, sorting unclear in case of binning higher than 1
lbin = int(metadata_LASP['LINE_BIN'])
lmin = int(metadata_LASP['UL_CORNER_LINE'])
lmax = int(metadata_LASP['LR_CORNER_LINE'])
# data starts at UL_CORNER, and fills LR_CORNER+1-UL_CORNER following pixels
lmin_val = lmin
numpx = int(np.ceil((lmax+1-lmin)/lbin))
lmax_val = int(lmin_val+numpx)

    
# get slit width
if 'LOW' in metadata_LASP['SLIT_STATE']:
    swidth = 1
else:
    swidth = 2

# CALIBRATE
print('%s Calibrating...' % time.strftime('%Y-%m-%d %H:%M:%S'))
# get calibration arrays
window_def = [metadata_LASP['UL_CORNER_BAND'], metadata_LASP['UL_CORNER_LINE'],
              metadata_LASP['LR_CORNER_BAND'], metadata_LASP['LR_CORNER_LINE']]
bin_def = [metadata_LASP['BAND_BIN'], metadata_LASP['LINE_BIN']]
wave, cal, calerr = UVIScalib.get_uvis_2015_calibration_interp(ettimes[0],
                                                               'FUV',
                                                               swidth,
                                                               window_def,
                                                               bin_def)

# perform calibration
valid_data_LASP = data_LASP
test_LASP = np.sum(valid_data_LASP, axis=-1)
plt.pcolormesh(np.log10(test_LASP.T))
plt.show()

lim = np.arange(40)
plt.scatter(np.arange(len(test[0,lim])), test[150,lim])
plt.show()
plt.scatter(np.arange(len(test_LASP[0,lim])), test_LASP[150,lim])
plt.show()
