import path_register

pr = path_register.PathRegister()
datapath = pr.datapath

import stitch_calibrate_project

import glob
import numpy as np
import os
import sys

# directory for all the preview stuff
mainsavepath = '%s/UVIS/PDS_UVIS/PREVIEW' % datapath
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)

# path where PDS data is stored
uvispath = '%s/UVIS/PDS_UVIS' % datapath

# list of available releases
rlslist = glob.glob('%s/*' % uvispath)
rlslist = [rlslist[iii] for iii in range(len(rlslist))
                if ('.' not in rlslist[iii] and 'COUVIS' in rlslist[iii])]

# txt about which release has been previewed already
previewlistfile = '%s/previewlist.txt' % mainsavepath
previewlist = np.array([[iii.split('\\')[-1],'unfinished'] for iii in rlslist])
try:
    previewlist_tmp = np.genfromtxt(previewlistfile, delimiter=',', dtype='U')
    for iii in range(len(previewlist[:,0])):
        tmp = np.where(previewlist[iii,0] == previewlist_tmp[:,0])[0]
        if np.size(tmp):
            previewlist[iii,1] = previewlist_tmp[tmp[0],1]
except:
    pass

# set up data processing
us = stitch_calibrate_project.UVISstitcher()

for rlspath in rlslist:
    # check for LASP flag-file
    LASP = True if len(glob.glob(os.path.join(rlspath,'LASP.txt'))) else False
    
    if not LASP:
        # get PDS observation list and skip non-aurora observations
        indexdata = np.genfromtxt('%s/INDEX/INDEX.TAB' % rlspath, delimiter=',',
                                  usecols=(0,4), dtype='U')
        files = [indexdata[iii,0].strip('"').strip()
                        for iii in range(len(indexdata[:,0]))
                        if (('SATURN' in indexdata[iii,1] or 'SOLAR WIND' in indexdata[iii,1]) and
                        'FUV' in indexdata[iii,0])]
        files = [iii.strip('.LBL') for iii in files]
    else:
        files = glob.glob(os.path.join(rlspath, '*.metadata'))
        files = [iii.split(uvispath)[-1].strip('.metadata') for iii in files]
    # set path where all the preview images and the txt list for projection 
    # processing will go    
    savepath = '%s/%s' % (mainsavepath,rlspath.split('\\')[-1])
    us.setSavePath(savepath)
    
    # create txt listing all possibly interesting observations if it doesn't exist already
    infofile = '%s/UVIS/PDS_UVIS/%s_info.txt' % (datapath,rlspath.split('\\')[-1])
    try:
        info = np.genfromtxt(infofile, delimiter=',', dtype='U')
    except:
        # set all tags to skip, sort out interesting data and change tags by hand
        info = [[files[iii], 'SKIP'] for iii in range(len(files))]
        np.savetxt(infofile, info, fmt='%s', delimiter=',')
    
    # if preview is already there
    if np.asscalar(previewlist[np.where(previewlist[:,0] == rlspath.split('\\')[-1])[0],1]) == 'done':
        continue
    
    # preview all interesting files
    fullfiles = ['%s%s' % (rlspath.split('\\')[0], files[iii])
                for iii in range(len(files))]
    us.previewFiles(fullfiles, LASP=LASP)
    
    # note down that preview has completed
    previewlist[np.where(previewlist[:,0] == rlspath.split('\\')[-1]),1] = 'done'
    np.savetxt(previewlistfile, previewlist, fmt='%s', delimiter=',')
    
    
