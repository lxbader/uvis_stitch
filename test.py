import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import plot_HST_UVIS
import UVIScalib
import time_conversions as tc

from datetime import datetime, timedelta
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
import rtree
from shapely.geometry import box,Polygon
import spiceypy as spice
import time

PROJ_RES = 4    # number of bins per degree lon
# twice the resolution in lat direction
lonnum = int(PROJ_RES*360)
latnum = int(PROJ_RES*360)
lonbins = np.linspace(0,360,num=lonnum+1)
latbins = np.linspace(-90,90,num=latnum+1)


# fill an rtree with lat-lon bins to speed up searching for intersections
# takes a while but has to run only once
print('%s Creating rtree...' % time.strftime('%Y-%m-%d %H:%M:%S'))
lonlat_rtree = rtree.index.Index()
ind = 0
perc = 0
for iii in range(len(lonbins)-1):
    if int(100*iii/(len(lonbins)-1))>perc+9:
        perc = int(100*iii/(len(lonbins)-1))
        print('Progress: %d %%' % perc)        
    for jjj in range(len(latbins)-1):        
        minx = lonbins[iii]
        miny = latbins[jjj]
        maxx = lonbins[iii+1]
        maxy = latbins[jjj+1]
        lonlat_rtree.insert(ind,
                            (minx, miny, maxx, maxy),
                            obj=(iii,jjj))
        ind += 1
        
# load Cassini SPICE kernels
print('%s Loading SPICE kernels...' % time.strftime('%Y-%m-%d %H:%M:%S'))
spice.kclear()
spice.furnsh('%s/SPICE/metakernels/cassini_generic.mk' % datapath)

# load pck kernel with +1100 km Saturn radius
pckpath = os.getcwd()
pck = '%s/saturn_1100.tpc' % pckpath
try:
    spice.kinfo(pck)
except spice.stypes.SpiceyError:
    spice.furnsh(pck)
# check whether the right numbers are loaded
RADII_1100 = spice.bodvrd("SATURN", "RADII", 3)[1]
if not np.all( RADII_1100 == np.array([61487,61487,55464])):
    print('Saturn +1100 radii could not be loaded')
    sys.exit()
    

uvispath = 'E:/data/UVIS/RAW/selec_PDS'  
uvispath = 'E:/data/UVIS/RAW/selec_PDS/HST_UVIS'
#uvispath = 'E:/data/UVIS/RAW/selec_PDS/notsogood'  
uvispath = 'D:/PhD/Data/UVIS/RAW'                

metadata = {}
metalabels = ['RECORD_TYPE','FILE_RECORDS','PRODUCT_ID','START_TIME','STOP_TIME',
              'INTEGRATION_DURATION','SLIT_STATE','^QUBE','AXES','AXIS_NAME',
              'CORE_ITEMS','CORE_ITEM_BYTES','CORE_ITEM_TYPE','CORE_BASE',
              'CORE_MULTIPLIER','CORE_UNIT','UL_CORNER_LINE','UL_CORNER_BAND',
              'LR_CORNER_LINE','LR_CORNER_BAND','BAND_BIN','LINE_BIN']

datfilelist = glob.glob('%s/*.DAT' % uvispath)
datfilelist = [datfilelist[iii] for iii in range(len(datfilelist)) if not 'CAL' in datfilelist[iii]]
lblfilelist = glob.glob('%s/*.LBL' % uvispath)
lblfilelist = [lblfilelist[iii] for iii in range(len(lblfilelist)) if not 'CAL' in lblfilelist[iii]]
calfilelist = glob.glob('%s/*.DAT' % uvispath)
calfilelist = [calfilelist[iii] for iii in range(len(calfilelist)) if 'CAL' in calfilelist[iii]]

# viewdir in UVIS_FUV frame
# returns [lon, lat] array
def proj_point(viewdir, ettime):
    try:
        center_proj_cart, garb, garb = spice.sincpt('ELLIPSOID',
                                                'SATURN',
                                                ettime,
                                                'CASSINI_KSM',                                                        
                                                'LT',                                                        
                                                'CASSINI',
                                                'CASSINI_UVIS_FUV',
                                                viewdir)
    except:
        return np.array([np.nan, np.nan])
    else:
        center_proj_sph = np.array(spice.reclat(center_proj_cart))
        center_proj_sph[1:] *= 180/np.pi
        center_proj_sph[1] = (center_proj_sph[1]+180) % 360
        
        return center_proj_sph[1:]
    

#for filectr in range(0,len(datfilelist)):
for filectr in range(1):
    # IMPORT DATA FILE
    # read lbl file
    print(datfilelist[filectr])
    print('%s Loading lbl file...' % time.strftime('%Y-%m-%d %H:%M:%S'))
    with open(lblfilelist[filectr],'r') as lblfile:
        line = lblfile.readline()
        while line:
            for label in metalabels:
                if label in line and '= ' in line:
                    value = line.strip('\n')
                    value = value.split('= ')[1]
                    value = value.strip('"')
                    value = value.strip('(')
                    value = value.strip(')')
                    value = value.strip(') ')
                    value = value.strip(' <SECOND>')
                    try:
                        metadata[label] = float(value)
                    except:
                        metadata[label] = value
            line = lblfile.readline()
    
    # get size of array
    axissize = [int(iii) for iii in metadata['CORE_ITEMS'].split(', ')]
    if not len(axissize)==3:
        print('Data array not 3D')
        sys.exit()
    data_fmt = '>(%d,%d,%d)u2' % (axissize[-1], axissize[-2], axissize[-3])
    if not axissize[-1] == metadata['FILE_RECORDS']:
        print('FILE_RECORDS does not fit array dimensions')
        sys.exit()
    
    # read dat file
    data = np.squeeze(np.fromfile(datfilelist[filectr], dtype=data_fmt, count=-1))
    
    # get timestamps
    tstart = datetime.strptime(metadata['START_TIME'], '%Y-%jT%H:%M:%S.%f')
    pytimes = np.array([tstart+iii*timedelta(seconds=metadata['INTEGRATION_DURATION'])
                        for iii in range(int(metadata['FILE_RECORDS'])+1)])
    ettimes = tc.datetime2et(pytimes)
    
    # get valid band indices
    bbin = int(metadata['BAND_BIN'])
    bmin = 0
    bmax = int((metadata['LR_CORNER_BAND']-metadata['UL_CORNER_BAND']+1)/bbin)
    
    # get valid line indices
    # no line_bin included yet, sorting unclear in case of binning higher than 1
    lbin = int(metadata['LINE_BIN'])
    lmin = int(metadata['UL_CORNER_LINE'])
    lmax = int(metadata['LR_CORNER_LINE']+1)
    if metadata['LINE_BIN'] > 1:
        print('LINE_BIN > 1')
        sys.exit()
        
    # get slit width
    if 'LOW' in metadata['SLIT_STATE']:
        swidth = 1
    else:
        swidth = 2
    
    # CALIBRATE
    print('BAND_BIN: %s' % bbin)
    print('%s Calibrating...' % time.strftime('%Y-%m-%d %H:%M:%S'))
    # get calibration arrays
    window_def = [metadata['UL_CORNER_BAND'], metadata['UL_CORNER_LINE'], metadata['LR_CORNER_BAND'], metadata['LR_CORNER_LINE']]
    bin_def = [metadata['BAND_BIN'], metadata['LINE_BIN']]
    wave, cal, calerr = UVIScalib.get_uvis_2015_calibration_interp(ettimes[0], 'FUV', swidth, window_def, bin_def)
    
    # perform calibration
    valid_data = data[:,lmin:lmax,bmin:bmax]    
    calib_data = np.zeros(np.shape(valid_data))
        
    for iii in range(int(metadata['FILE_RECORDS'])):
        tmp = valid_data[iii,:,:]*cal
        calib_data[iii,:,:] = tmp
            
    # integrate over frequency
    intmin = int(np.floor(0.25*bmax))
    intmax = int(np.floor(0.75*bmax))
    bdiff = np.mean(np.diff(wave))  # it's a linear scale so we can simplify a bit
    int_data = np.full((int(metadata['FILE_RECORDS']), lmax-lmin), np.nan)
    for recctr in range(int(metadata['FILE_RECORDS'])):
        for linectr in range(lmax-lmin):
            int_data[recctr,linectr] = np.sum(calib_data[recctr,linectr,intmin:intmax])*bdiff
    
    if filectr == 0:
        plt.pcolormesh(int_data[:200,:].T)
    
    # Get UVIS_FUV FOV (NAIF ID of UVIS_FUV -82840, max number of vectors returned)
    shape, frame, boresight, n_boundvec, boundvec = spice.getfov(-82840, 100)

    # get all angles between corner points (assuming we get 4 corner points and a rectangular FOV)
    if shape != 'RECTANGLE' or len(boundvec) != 4:
        print('Warning: unexpected FOV return')
        sys.exit()        
    allangles = np.array([])
    for iii in range(len(boundvec)):
        for jjj in range(iii+1, len(boundvec)):
            allangles = np.append(allangles, np.arccos(np.dot(boundvec[iii], boundvec[jjj])))
    allangles = np.sort(np.unique(allangles))
    # short side
    swidth_rad = allangles[0]
    # long side
    slength_rad = allangles[1]
    # 3rd value (diagonal) is dropped
    
    # get corner points between detector pixels
    numpx = np.shape(calib_data)[1]
    allcorners = np.full((numpx+1,2,3), np.nan)
    cornerbins = np.arange(lmin-32,lmax+1-32,lbin)
    for iii in range(len(cornerbins)):
        allcorners[iii,0,:] = spice.rotvec(spice.rotvec(boresight,swidth_rad/2,2),slength_rad/64*cornerbins[iii],1)
        allcorners[iii,1,:] = spice.rotvec(spice.rotvec(boresight,-swidth_rad/2,2),slength_rad/64*cornerbins[iii],1)
            
    # determine the 1 center and 4 corner viewing directions of each pixel in the UVIS_FUV frame
    FOVcenters = np.full((numpx,3), np.nan)
    FOVcorners = np.full((numpx,4,3), np.nan)
    centerbins = cornerbins[:-1]+np.diff(cornerbins)/2
    for iii in range(numpx):
        FOVcorners[iii,:2,:] = allcorners[iii,:,:]
        FOVcorners[iii,2:,:] = allcorners[iii+1,::-1,:]        
        FOVcenters[iii,:] = spice.rotvec(boresight,slength_rad/64*centerbins[iii],1)
        
    # determine whether to split the image
    #
    #
    #
    #
    
    # empty intensity sums and pixel numbers
    proj_sum = np.zeros((len(lonbins)-1, len(latbins)-1))
    proj_num = np.zeros((len(lonbins)-1, len(latbins)-1))
    
    # loop over all UVIS pixels and records
    print('%s Projecting...' % time.strftime('%Y-%m-%d %H:%M:%S'))
    perc = 0
    
    for recctr in range(int(metadata['FILE_RECORDS'])):
        if int(100*recctr/int(metadata['FILE_RECORDS'])) > perc+9:
            perc = int(100*recctr/int(metadata['FILE_RECORDS']))
            print('Progress: %d %%' % perc)
        for pxctr in range(numpx):
            if int_data[recctr, pxctr] == 0:
                continue
            # get surface intercept point for pixel boresight
            tmpcenter = proj_point(FOVcenters[pxctr,:], ettimes[recctr])
            if np.any(np.isnan(tmpcenter)):
                continue
            # get surface intercept for pixel corners
            tmpcorners = np.array([proj_point(FOVcorners[pxctr,iii,:], ettimes[recctr]) for iii in range(4)])
            if np.any(np.isnan(tmpcorners)):
                continue            
            # check for bins crossing the 360-0 longitude boundary
            # rotate them by lon=180deg, perform calculations, rotate back
            datechangeflag = np.max(np.abs(np.diff(tmpcorners[:,0]))) > 270
            if datechangeflag:           
                # drop the pole pixels
                if np.sum(tmpcorners[:,1]>89)>2:
                    continue
                tmpcorners[:,0] += 180
                tmpcorners[:,0] = tmpcorners[:,0]%360

            # create projected pixel polygon from corners
            poly = Polygon(tmpcorners)
            # find bins which might intersect (only rectangular boundaries)
            closebins = lonlat_rtree.intersection(poly.bounds, objects=True)
            binlist = np.array([n.object for n in closebins])
            if not np.any(binlist):
                continue
            # iterate through all lon-lat bin candidates, check exact shapes
            for ijk in range(len(binlist)):
                # get lon-lat bin polygon
                arglon, arglat = binlist[ijk]                    
                binbox = box(lonbins[arglon], latbins[arglat],
                              lonbins[arglon+1], latbins[arglat+1])
                # intersect with projected pixel polygon
                valid = binbox.intersects(poly)
                if not valid:
                    continue
                # prepare for averaging if multiple pixels are in one bin
                # could be improved by taking into account intersection areas
                if datechangeflag:
                    # bring pixels intersecting the date border back to their
                    # original location
                    tmplon = int((arglon+(len(lonbins)-1)/2) % (len(lonbins)-1))
                    proj_sum[tmplon,arglat] += int_data[recctr, pxctr]
                    proj_num[tmplon,arglat] += 1
                else:
                    proj_sum[arglon,arglat] += int_data[recctr, pxctr]
                    proj_num[arglon,arglat] += 1
    print('%s Projection finished' % time.strftime('%Y-%m-%d %H:%M:%S'))
    
    proj = proj_sum/proj_num
            
    plt.pcolormesh(int_data.T)
    plt.show()
    plt.close()
    
    plt.pcolormesh(np.log10(proj_sum.T))
    plt.show()
    plt.close()
    
    if np.any(proj[:,:int(60*PROJ_RES)]>0):
        plotproj = proj[:,:int(60*PROJ_RES)]
    else:
        plotproj = proj[:,-int(60*PROJ_RES):]
        plotproj = plotproj[:,::-1]
        
    plt.pcolormesh(np.log10(plotproj.T))
    plt.show()
    plt.close()
        
    plot_HST_UVIS.simpleplot(plotproj.T, 'D:/proj_%s' % metadata['^QUBE'].split('.')[0], 'Projection test', 'LT', 50)
        
