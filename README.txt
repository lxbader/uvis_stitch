==================================================================================
UVIS stitching and projecting of auroral imagery onto a latitude-longitude grid
---- for use with PDS datasets ----
==================================================================================
Author:
A. Bader, Lancaster University, a.bader@lancaster.ac.uk
==================================================================================
Following is a very short description of the important bits of this code. For a 
proper understanding it's gonna be necessary to have a close look at it and hope
that there are comments for the important steps (spoiler: there aren't too many of
them). Feel free to get in touch with comments/corrections/questions.

The code should run somewhat smoothly and reliably, albeit a bit slow. Significant
speed gain available by not projecting in a small circle around the pole. But then,
it's not like one needs to do it too often anyway.

NAIF0012.TLS:
Just the most recent SPICE LSK kernel, used for converting between Python time
formats and ephemeris time.

PATH_REGISTER:
Small class helping me locate data locations and other paths on different 
machines.

PREVIEW_RELEASE:
Quickly plots all raw data of a release in a pcolormesh and saves it for reference.
Helps with picking out interesting observations from all the other stuff.
Creates a list of all datafiles, with all processing commands set to "SKIP". To
calibrate and project a file, simply edit the command to 
"COMBINE_START" ... First file to put into a single projection.
"CONT" ............ Following files going into the same projection.
"COMBINE_STOP" .... Last file to put into the same projection.
"SPLIT_AUTO" ...... Auto splits a single datafile into several projections using
................... the rate of change in UVIS pointing direction. Stripy return
................... scans are excluded. Functionality for splitting by hand (giving
................... max/min register numbers) is given, see code for details
"SINGLE"........... Makes a single projection out of one datafile. Auto-removes
................... stripy return scans.
"SINGLE_NOCLEAN"... Same as above, without removing return scans.
Any other command is being ignored. Then run PROCESS_QUEUE_FROM_TXT

PROCESS_QUEUE_FROM_TXT:
Processes data files according the the txt lists created by PREVIEW_RELEASE and 
flagged by hand.

SATURN_1100.TPC:
!!! Take care with this one if you're using SPICE for something else in combination
with this code !!!
Redefines the SPICE radius of Saturn to a 1 bar + 1100 km level (approximate
altitude of auroral emission) so that we can use the SPICE calculation of the
surface intercept (sincpt) of the UVIS viewing direction with the "auroral layer".

STITCH_CALIBRATE_PROJECT:
Class for handling all the calculations. Imports data, calibrates it and projects it
onto a longitude-latitude grid of customisable resolution. Can output the projected 
data in integrated form as well as in full spectral resolution (beware of large file
sizes). Files are saved in fits format.

TIME_CONVERSIONS:
Some useful functions for converting between different time keeping.

UVIS_CALIB:
FUV calibration routine, directly translated from available IDL calibration routines.
No EUV calibration implemented, some other parts which weren't needed for this
project are missing as well. Results have been checked and found to agree with IDL
outputs.
