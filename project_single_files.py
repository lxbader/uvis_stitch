import path_register

pr = path_register.PathRegister()
datapath = pr.datapath

import stitch_calibrate_project

import glob
import numpy as np
import os
import sys

us = stitch_calibrate_project.UVISstitcher()
mainsavepath = '{}/UVIS/PDS_UVIS/PROJECTIONS_TEST'.format(datapath)
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)

us.setSavePath(mainsavepath)
us.setResolution(2)
us.saveSpecs(False)

couvis = '58'
filename = 'FUV2017_080_21_25'

LASP = True if int(couvis)==60 else False

if LASP:
    file = glob.glob('{}/UVIS/PDS_UVIS/COUVIS_00{}/{}*'.format(datapath, couvis, filename), recursive=True)
else:
    file = glob.glob('{}/UVIS/PDS_UVIS/COUVIS_00{}/DATA/**/{}*.DAT'.format(datapath, couvis, filename), recursive=True)
file = file[0].split('.')[0]

#us.stitchFiles([file], LASP=LASP, clean=False)
#us.splitFile(file, LASP=LASP, sens=5)
    




#%%

#import UVIScalib
#from datetime import datetime
#import time_conversions as tc
#time = tc.datetime2et(datetime.strptime('2017_257_15_21', '%Y_%j_%H_%M'))
#swidth = 1
#window_def = [us.metadata['UL_CORNER_BAND'], us.metadata['UL_CORNER_LINE'],
#                      us.metadata['LR_CORNER_BAND'], us.metadata['LR_CORNER_LINE']]
#bin_def = [us.metadata['BAND_BIN'], us.metadata['LINE_BIN']]
#wave, cal, calerr = UVIScalib.get_uvis_2015_calibration_interp(time,
#                                                                       'FUV',
#                                                                       swidth,
#                                                                       window_def,
#                                                                       bin_def)
#
#np.savetxt('E:/cal_{}.txt'.format(filename), cal)
#
#gitpath = pr.gitpath
#sys.path.insert(0, '%s/cassinipy' % gitpath)
#
#import get_image
#
#image, _, _ = get_image.getRedUVIS(r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0060\2017_257T15_21_54.fits')
#    