import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import UVIScalib
import time_conversions as tc

from astropy.io import fits
import cubehelix
from datetime import datetime, timedelta, timezone
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import rtree
import scipy.interpolate as sint
from shapely.geometry import box,Polygon
import spiceypy as spice
import time

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)

class StitchError(Exception):
    def __init__(self, msg=None):
        super(StitchError, self).__init__(msg)
        
class SpiceError(StitchError):
    def __init__(self, msg=None):
        super(SpiceError, self).__init__(msg)
        
class DataError(StitchError):
    def __init__(self, msg=None):
        super(DataError, self).__init__(msg)        

class UVISstitcher(object):
    
    def __init__(self):
        # path to where projected images are saved
        self.savepath = None
        
        # set whether to save spectral information
        self.savespecs = False
        
        # set resolution
        self.PROJ_RES = None
        self.setResolution(2) 
        
        # integration limits
        self.intmethod = 'GUSTIN_2016'
        
        # initialize SPICE
        self.initSpice()
        
        # current UVIS datafile
        self.datfile = None
        # metadata of current UVIS datafile
        self.metadata = {}
        
        # time vector with time of each record
        self.ettimes = None 
        
        # FOV corner and center vectors for each pixel
        self.FOVcenters = None
        self.FOVcorners = None
        
        self.resetData()        
        self.resetProjection()
        
    def resetData(self):
         # current calibrated data [records, pixels, wavelength bins]
        self.calib_data = None
        # current integrated data [records, line pixels]
        self.int_data = None
        # number of pixels per line
        self.numpx = None        
        
    def resetProjection(self):
        self.proj_sum = np.zeros((self.lonnum, self.latnum))
        self.proj_num = np.zeros((self.lonnum, self.latnum))
        self.proj_minangle = np.full((self.lonnum, self.latnum), np.nan)
        self.proj_spec_sum = False
        self.proj_spec_num = False        
        self.starttime = None
        self.stoptime = None        
        self.fullnumrec = 0
        self.singleexp = None
        self.totalexp = 0
        
    def setSavePath(self, savepath):
        if not os.path.exists(savepath):
            os.makedirs(savepath)
        self.savepath = savepath
        
    def saveSpecs(self, value):
        self.savespecs = value
        
    def setResolution(self, binsperdeglon):
        if binsperdeglon != self.PROJ_RES:
            self.PROJ_RES = binsperdeglon           
            # determine binning and size of projected image
            self.lonnum = int(self.PROJ_RES*360)
            self.latnum = int(self.PROJ_RES*360)
            self.lonbins = np.linspace(0,360,num=self.lonnum+1)
            self.latbins = np.linspace(-90,90,num=self.latnum+1)        
            # rtree with lon-lat binning for fast intersection searching
            self.lonlat_rtree = None        
    
    def setIntegrationMethod(self, method):
        self.intmethod = method
        
    def makeRtree(self):
        # fill an rtree with lat-lon bins to speed up searching for intersections
        # takes a while but has to run only once
        print('%s Creating rtree...' % time.strftime('%Y-%m-%d %H:%M:%S'))
        self.lonlat_rtree = rtree.index.Index()
        ind = 0
        perc = 0        
        
        iiirange = np.arange(len(self.lonbins)-1)
        jjjrange = np.arange(len(self.latbins)-1)
        
        if False:
            # select small lat-lon space for super hi res projections
            lonmin = 12
            lonmax = 73
            latmin = 67
            latmax = 78
            iiirange = np.where((self.lonbins>=lonmin) & (self.lonbins <lonmax))[0]
            jjjrange = np.where((self.latbins>=latmin) & (self.latbins <latmax))[0]
        
        for iii in iiirange:
            thisperc = 100*np.argwhere(iiirange == iii)[0][0]/len(iiirange)
            if thisperc>perc+10:
                perc += 10
                print('Progress: %d %%' % perc)        
            for jjj in jjjrange:        
                minx = self.lonbins[iii]
                miny = self.latbins[jjj]
                maxx = self.lonbins[iii+1]
                maxy = self.latbins[jjj+1]
                self.lonlat_rtree.insert(ind,
                                    (minx, miny, maxx, maxy),
                                    obj=(iii,jjj))
                ind += 1
    
    def initSpice(self):
        # load Cassini SPICE kernels
        print('%s Loading SPICE kernels...' % time.strftime('%Y-%m-%d %H:%M:%S'))
        spice.kclear()
        spice.furnsh('%s/SPICE/metakernels/cassini_generic.mk' % datapath)
        
        # load pck kernel with +1100 km Saturn radius
        pck = '%s/uvis_stitch/saturn_1100.tpc' % gitpath
        try:
            spice.kinfo(pck)
        except spice.stypes.SpiceyError:
            spice.furnsh(pck)
        # check whether the right numbers are loaded
        RADII_1100 = spice.bodvrd("SATURN", "RADII", 3)[1]
        if not np.all(RADII_1100 == np.array([61487,61487,55464])):
            raise SpiceError('Saturn +1100 radii could not be loaded')
        
    def getFovVectors(self):
        lbin = int(self.metadata['LINE_BIN'])
        lmin = int(self.metadata['UL_CORNER_LINE'])
        lmax = int(self.metadata['LR_CORNER_LINE']+1)
        # Get UVIS_FUV FOV (NAIF ID of UVIS_FUV -82840, max number of vectors returned)
        shape, frame, boresight, n_boundvec, boundvec = spice.getfov(-82840, 100)
    
        # get all angles between corner points (assuming we get 4 corner points and a rectangular FOV)
        if shape != 'RECTANGLE' or len(boundvec) != 4:
            raise SpiceError('Unexpected FOV return')
        allangles = np.array([])
        for iii in range(len(boundvec)):
            for jjj in range(iii+1, len(boundvec)):
                allangles = np.append(allangles, np.arccos(np.dot(boundvec[iii], boundvec[jjj])))
        allangles = np.sort(np.unique(allangles))
        # short side
        swidth_rad = allangles[0]
        # long side
        slength_rad = allangles[1]
        # 3rd value (diagonal) is dropped
        
        # get corner points between detector pixels
        allcorners = np.full((self.numpx+1,2,3), np.nan)
        cornerbins = np.arange(lmin-32,lmax+1-32,lbin)
        for iii in range(len(cornerbins)):
            allcorners[iii,0,:] = spice.rotvec(spice.rotvec(boresight,swidth_rad/2,2),slength_rad/64*cornerbins[iii],1)
            allcorners[iii,1,:] = spice.rotvec(spice.rotvec(boresight,-swidth_rad/2,2),slength_rad/64*cornerbins[iii],1)
                
        # determine the 1 center and 4 corner viewing directions of each pixel in the UVIS_FUV frame
        self.FOVcenters = np.full((self.numpx,3), np.nan)
        self.FOVcorners = np.full((self.numpx,4,3), np.nan)
        centerbins = cornerbins[:-1]+np.diff(cornerbins)/2
        for iii in range(self.numpx):
            self.FOVcorners[iii,:2,:] = allcorners[iii,:,:]
            self.FOVcorners[iii,2:,:] = allcorners[iii+1,::-1,:]        
            self.FOVcenters[iii,:] = spice.rotvec(boresight,slength_rad/64*centerbins[iii],1)
            
    # import data, calibrate and integrate
    def getCalibIntegData(self, file, LASP=False, ignoreSingleRecs=True):
        self.resetData()
        spice.reset()
        self.metadata = {}
        print(file)
        metalabels = ['RECORD_TYPE','FILE_RECORDS','PRODUCT_ID','START_TIME','STOP_TIME',
              'INTEGRATION_DURATION','SLIT_STATE','^QUBE','AXES','AXIS_NAME',
              'CORE_ITEMS','CORE_ITEM_BYTES','CORE_ITEM_TYPE','CORE_BASE',
              'CORE_MULTIPLIER','CORE_UNIT','UL_CORNER_LINE','UL_CORNER_BAND',
              'LR_CORNER_LINE','LR_CORNER_BAND','BAND_BIN','LINE_BIN']
        
        if LASP:
            metalabels = [iii.replace('_', ' ') for iii in metalabels]
            datfile = '%s' % file
            lblfile = '%s.metadata' % file
        else:
            datfile = '%s.dat' % file
            lblfile = '%s.lbl' % file
        
        # IMPORT DATA FILE
        # read lbl file
        
        with open(lblfile,'r') as lf:
            line = lf.readline()
            while line:
                for label in metalabels:
                    if label in line and '= ' in line:
                        value = line.strip('\n')
                        value = value.split('= ')[1]
                        value = value.strip('"')
                        value = value.strip('(')
                        value = value.strip(')')
                        value = value.strip(') ')
                        value = value.strip(' <SECOND>')
                        if LASP:
                            tmp = label.replace(' ','_')
                        else:
                            tmp = label
                        try:
                            self.metadata[tmp] = float(value)
                        except:
                            self.metadata[tmp] = value
                line = lf.readline()
        
        # skip files with only one record
        if self.metadata['FILE_RECORDS'] < 2 and ignoreSingleRecs:
            raise DataError('Only one file record')
        # skip files with insane pixel binning
        if self.metadata['LINE_BIN'] >30:
            raise DataError('Too high pixel binning')
        # skip files with unknown integration time
        if type(self.metadata['INTEGRATION_DURATION']) != float:
            raise DataError('Unknown integration duration')
            
        # get timestamps
        tstart = datetime.strptime(self.metadata['START_TIME'], '%Y-%jT%H:%M:%S.%f')
        pytimes = np.array([tstart+iii*timedelta(seconds=self.metadata['INTEGRATION_DURATION'])
                            for iii in range(int(self.metadata['FILE_RECORDS'])+1)])
        self.ettimes = tc.datetime2et(pytimes)
        
        # get valid band indices
        bbin = int(self.metadata['BAND_BIN'])
        bmin = 0
        bmax = int(np.ceil((self.metadata['LR_CORNER_BAND']-self.metadata['UL_CORNER_BAND']+1)/bbin))
        self.numbx = bmax
        
        # get valid line indices
        # no line_bin included yet, sorting unclear in case of binning higher than 1
        lbin = int(self.metadata['LINE_BIN'])
        lmin = int(self.metadata['UL_CORNER_LINE'])
        lmax = int(self.metadata['LR_CORNER_LINE'])
        # data starts at UL_CORNER, and fills LR_CORNER+1-UL_CORNER following pixels
        lmin_val = lmin
        self.numpx = int(np.ceil((lmax+1-lmin)/lbin))
        lmax_val = int(lmin_val+self.numpx)
            
        # get slit width
        if 'LOW' in self.metadata['SLIT_STATE']:
            swidth = 1
        else:
            swidth = 2
        
        # CALIBRATE
        print('%s Calibrating...' % time.strftime('%Y-%m-%d %H:%M:%S'))
        # get calibration arrays
        window_def = [self.metadata['UL_CORNER_BAND'], self.metadata['UL_CORNER_LINE'],
                      self.metadata['LR_CORNER_BAND'], self.metadata['LR_CORNER_LINE']]
        bin_def = [self.metadata['BAND_BIN'], self.metadata['LINE_BIN']]
        self.wave, cal, calerr = UVIScalib.get_uvis_2015_calibration_interp(self.ettimes[0],
                                                                       'FUV',
                                                                       swidth,
                                                                       window_def,
                                                                       bin_def)
        
        # get size of array
        axissize = [int(iii) for iii in self.metadata['CORE_ITEMS'].split(', ')]
        if not len(axissize)==3:
            raise DataError('Data array not 3D') 
        if not axissize[-1] == self.metadata['FILE_RECORDS']:
            raise DataError('FILE_RECORDS does not fit array dimensions')
            
        # read dat file
        if LASP:
            # expected filesize
            exp = 2*axissize[-1]*self.numpx*self.numbx
            act = os.path.getsize(datfile)
            data_fmt = '>({0:d},{1:d},{2:d})u2'.format(axissize[-1],self.numpx,self.numbx)
            f = open(datfile, 'rb')
            f.seek(act-exp, os.SEEK_SET)
            self.valid_data = np.squeeze(np.fromfile(f, dtype=data_fmt, count=-1))
            f.close()
        else:
            data_fmt = '>(%d,%d,%d)u2' % (axissize[-1], axissize[-2], axissize[-3])
            data = np.squeeze(np.fromfile(datfile, dtype=data_fmt, count=-1))
            self.valid_data = data[:,lmin_val:lmax_val,bmin:bmax]
        
        # perform calibration
        # turn counts into counts/sec (or kR/A?)
        self.valid_data = np.array(self.valid_data, dtype=np.float32)
        self.valid_data /= self.metadata['INTEGRATION_DURATION']
        self.calib_data = np.zeros(np.shape(self.valid_data))            
        for iii in range(int(self.metadata['FILE_RECORDS'])):
            tmp = self.valid_data[iii,:,:]*cal
            self.calib_data[iii,:,:] = tmp
        
        # get band width
        bdiff = np.mean(np.diff(self.wave))  # it's a linear scale so we can simplify a bit
        # empty array for integrated data
        self.int_data = np.full((int(self.metadata['FILE_RECORDS']), self.numpx), np.nan)
        # integrate over frequency
        if self.intmethod == 'GUSTIN_OLD_SIMPLIFIED':
            ly_alpha = 1215.67
            # which wavelength bins do not include ly-alpha
            lower = np.where(self.wave+1/2*bdiff < ly_alpha)[0]
            upper = np.where(self.wave-1/2*bdiff > ly_alpha)[0]
            ndx_la = np.append(lower, upper)
            # wavelength limits Grodent 2011 (get rid of reflected sunlight stuff and such)
            min_lim = 1200
            max_lim = 1630
            ndx_sun = np.where((self.wave>min_lim)*(self.wave<max_lim))
            # valid bins
            ndx_val = np.intersect1d(ndx_la, ndx_sun)
            
            for recctr in range(int(self.metadata['FILE_RECORDS'])):
                for linectr in range(np.shape(self.calib_data)[1]):
                    self.int_data[recctr,linectr] = np.sum(self.calib_data[recctr,linectr,ndx_val])*bdiff
            # multiplicator reflecting that we didn't use the whole UVIS wavelength range
            self.int_data *= len(self.calib_data[0,0,:])/len(ndx_val)
            # see Gustin 2012, Conversion from HST ACS and STIS auroral counts into
            # brightness, precipitated power, and radiated power
            # for H2 giant planets
            self.int_data *= 1.39
        elif self.intmethod in ['GUSTIN_2016', 'DAYGLOW', 'REFL_SUNLIGHT']:
            # desired wavelength range (A)
            if self.intmethod == 'GUSTIN_2016':
                min_lim = 1550
                max_lim = 1620
            elif self.intmethod == 'DAYGLOW':
                min_lim = 1250
                max_lim = 1550
            elif self.intmethod == 'REFL_SUNLIGHT':
                min_lim = 1650
                max_lim = 1800
            # interpolate to find out which bins we need
            f_int = sint.interp1d(self.wave,np.arange(len(self.wave)))
            min_bin = f_int(min_lim)
            max_bin = f_int(max_lim)
            # for each bin, find out which fraction overlaps with the desired range
            multipliers = np.zeros((len(self.wave)))
            for iii in range(len(multipliers)):
                left = np.max([iii-0.5,min_bin])
                right = np.min([iii+0.5,max_bin])
                if right<left:
                    continue
                else:
                    multipliers[iii] = right-left
            # integrate
            for recctr in range(int(self.metadata['FILE_RECORDS'])):
                for linectr in range(np.shape(self.calib_data)[1]):
                    # multiply each spectrum with its overlap multiplicators
                    # multiply with bin width in A (constant width), kR/A -> kR
                    self.int_data[recctr,linectr] = np.sum(self.calib_data[recctr,linectr,:]*multipliers*bdiff)
            if self.intmethod == 'GUSTIN_2016':
                # according to Gustin 2016 Jupiter, conversion factor to get
                # full UV range 700-1700 A
                self.int_data *= 8.1
        else:
            raise DataError('Unknown integration method')
        # if a pixel is zero throughout most records, nan it
        for iii in range(np.shape(self.int_data)[1]):
            if np.nansum(self.int_data[:,iii] <= 0)/len(self.int_data[:,iii]) > 0.8:
                self.int_data[:,iii] = np.nan
        # if a record is zero throughout most pixels, nan it
        for iii in range(np.shape(self.int_data)[0]):
            if np.nansum(self.int_data[iii,:] <= 0)/len(self.int_data[iii,:]) > 0.8:
                self.int_data[iii,:] = np.nan

    # viewdir in UVIS_FUV frame
    # returns [lon, lat] array and [incidence angle]
    def proj_point(self, viewdir, ettime):
        try:
            center_proj_cart, garb, garb = spice.sincpt('ELLIPSOID','SATURN',
                                                        ettime,'CASSINI_KSM',
                                                        'LT+S','CASSINI',
                                                        'CASSINI_UVIS_FUV',
                                                        viewdir)
        except:
            return np.array([np.nan, np.nan]), np.nan
        else:
            center_proj_sph = np.array(spice.reclat(center_proj_cart))
            center_proj_sph[1:] *= 180/np.pi
            # 0 LT/lon starts at -X axis
            center_proj_sph[1] = (center_proj_sph[1]+180) % 360 
            # calculate viewing angle
            # get surface normal
            RADII_1100 = spice.bodvrd("SATURN", "RADII", 3)[1]
            sfnm = spice.surfnm(RADII_1100[0],
                                RADII_1100[1],
                                RADII_1100[2],
                                center_proj_cart)
            viewdir_KSM = spice.mxv(spice.pxform('CASSINI_UVIS_FUV',
                                                 'CASSINI_KSM',
                                                 ettime),
                                    viewdir)
            radangle = spice.vsep(viewdir_KSM,sfnm)-np.pi/2
            return center_proj_sph[1:], radangle/np.pi*180
        spice.reset()

    def projectCurrent(self, recmin='auto', recmax='auto', reclist=[]):
        # make rtree if it's not there already
        if self.lonlat_rtree == None:
            self.makeRtree()
        print('%s Projecting...' % time.strftime('%Y-%m-%d %H:%M:%S'))
        # determine over which records to loop
        if not len(reclist):
            if recmin == 'auto':
                recmin = 0
            if recmax == 'auto' or recmax>=self.metadata['FILE_RECORDS']:
                recmax = int(self.metadata['FILE_RECORDS'])-2
            reclist = np.arange(recmin, recmax+1)
        print('Limits:', recmin, recmax)
        # make empty array for full unintegrated intensity distribution
        if self.savespecs:
            if not np.any(self.proj_spec_sum):
                self.proj_spec_sum = np.zeros((self.lonnum, self.latnum, len(self.wave)))
                self.proj_spec_num = np.zeros((self.lonnum, self.latnum, len(self.wave)))
        # loop over all UVIS pixels and records
        perc = 0
        print('Progress: %d %%' % perc)
        
        for recctr in reclist:
            tmp = 100*(recctr-reclist[0])/(reclist[-1]-reclist[0])
            if tmp > perc+9:
                perc = tmp
                print('Progress: %d %%' % perc)
            for pxctr in range(self.numpx-1):
                if np.isnan(self.int_data[recctr, pxctr]):
                #or self.int_data[recctr, pxctr]==0:
                    continue
                # get surface intercept point for pixel boresight
                # and incident angle
                tmpcenter, centerangle = self.proj_point(self.FOVcenters[pxctr,:], self.ettimes[recctr])
                if np.any(np.isnan(tmpcenter)):
                    continue
                # get surface intercept for pixel corners
                tmpcorners = np.array([self.proj_point(self.FOVcorners[pxctr,iii,:], self.ettimes[recctr])[0] for iii in range(4)])
                if np.any(np.isnan(tmpcorners)):
                    continue
                # create projected pixel polygon in polar view
                tmplon = tmpcorners[:,0]*np.pi/180
                tmplat = 90-np.abs(tmpcorners[:,1])
                xy_pixel = np.array([[tmplat[iii]*np.cos(tmplon[iii]),
                                tmplat[iii]*np.sin(tmplon[iii])]
                        for iii in range(4)])
                poly_polar = Polygon(xy_pixel)
                ########################
                # fake polar pixel to check whether pixel hits pole
                bin_xy = np.array([[0.1*np.cos(np.pi/2*iii),
                                    0.1*np.sin(np.pi/2*iii)]
                                        for iii in range(4)])
                binbox = Polygon(bin_xy)
                polarflag = binbox.intersects(poly_polar)
                
#                # handle the pole pixels
#                polarflag = False
#                if np.any([np.any(np.abs(tmpcorners[:,1])>86), np.abs(tmpcenter[1])>86]):
#                    polarflag = True
                #############################
                    
                # check for non-polar bins crossing the 360-0 longitude boundary
                # rotate them by lon=180deg, perform calculations, rotate back
                datechangeflag = ((np.max(tmpcorners[:,0])-np.min(tmpcorners[:,0])> 270)
                                    *(not polarflag))
                if datechangeflag:           
                    tmpcorners[:,0] += 180
                    tmpcorners[:,0] = tmpcorners[:,0]%360
    
                # create projected pixel polygon from corners
                poly = Polygon(tmpcorners)
                # find bins which might intersect (only rectangular boundaries)
                if polarflag:
                    if tmpcenter[1]>0:
                        closebins = self.lonlat_rtree.intersection([
                                0,
                                np.min(tmpcorners[:,1]),
                                360,
                                90], objects=True)
#                                np.max(tmpcorners[:,1])+1/2/self.PROJ_RES], objects=True)
                    else:
                        closebins = self.lonlat_rtree.intersection([
                                0,
                                -90,
#                                np.min(tmpcorners[:,1])-1/2/self.PROJ_RES,
                                360,
                                np.max(tmpcorners[:,1])], objects=True)
                else:
                    closebins = self.lonlat_rtree.intersection(poly.bounds, objects=True)
                binlist = np.array([n.object for n in closebins])
                if not np.any(binlist):
                    continue
                
                # iterate through all lon-lat bin candidates, check exact shapes
                for ijk in range(len(binlist)):
                    # get lon-lat bin polygon
                    arglon, arglat = binlist[ijk]
                    if polarflag:
                        bincorners = np.array([[self.lonbins[arglon], self.latbins[arglat]],
                                               [self.lonbins[arglon], self.latbins[arglat+1]],
                                               [self.lonbins[arglon+1], self.latbins[arglat+1]],
                                               [self.lonbins[arglon+1], self.latbins[arglat]]])
                        tmplon = bincorners[:,0]*np.pi/180
                        tmplat = 90-np.abs(bincorners[:,1])
                        bin_xy = np.array([[tmplat[iii]*np.cos(tmplon[iii]),
                                        tmplat[iii]*np.sin(tmplon[iii])]
                                        for iii in range(4)])
                        binbox = Polygon(bin_xy)
                        valid = binbox.intersects(poly_polar)
                    else:
                        binbox = box(self.lonbins[arglon], self.latbins[arglat],
                                      self.lonbins[arglon+1], self.latbins[arglat+1])
                        # intersect with projected pixel polygon
                        valid = binbox.intersects(poly)
                    if not valid:
                        continue
                    # prepare for averaging if multiple pixels are in one bin
                    # could be improved by taking into account intersection areas
                    if datechangeflag:
                        # bring pixels intersecting the date border back to their
                        # original location
                        tmplon = int((arglon+(len(self.lonbins)-1)/2) % (len(self.lonbins)-1))
                        self.proj_sum[tmplon,arglat] += self.int_data[recctr, pxctr]
                        self.proj_num[tmplon,arglat] += 1
                        if np.isnan(self.proj_minangle[tmplon,arglat]) or centerangle<self.proj_minangle[tmplon,arglat]:
                            self.proj_minangle[tmplon,arglat] = centerangle 
                        # add unintegrated data where valid
                        if self.savespecs:
                            valid = np.where(np.isfinite(self.calib_data[recctr,pxctr]))[0]
                            self.proj_spec_sum[tmplon,arglat,valid] += self.calib_data[recctr,pxctr,valid]
                            self.proj_spec_num[tmplon,arglat,valid] += 1
                    else:
                        self.proj_sum[arglon,arglat] += self.int_data[recctr, pxctr]
                        self.proj_num[arglon,arglat] += 1
                        if np.isnan(self.proj_minangle[arglon,arglat]) or centerangle<self.proj_minangle[arglon,arglat]:
                            self.proj_minangle[arglon,arglat] = centerangle
                        if self.savespecs:
                            valid = np.where(np.isfinite(self.calib_data[recctr,pxctr]))[0]
                            self.proj_spec_sum[arglon,arglat,valid] += self.calib_data[recctr,pxctr,valid]
                            self.proj_spec_num[arglon,arglat,valid] += 1
        print('%s Projection finished' % time.strftime('%Y-%m-%d %H:%M:%S'))
        
    def setHeader(self, hdr, style='INT'):
        # write up header
        hdr.set('DATE',
                       datetime.now(timezone.utc).strftime('%Y-%m-%d %H:%M:%S'),
                       'Creation UTC of FITS header')
        hdr.set('CREATOR',
                       'A. BADER',
                       'Creator of this FITS file')
        hdr.set('STARTUTC',
                       datetime.strftime(tc.et2datetime(self.starttime),
                                                   '%Y-%m-%d %H:%M:%S'),
                       'Start time of observation')
        hdr.set('STOPUTC',
                       datetime.strftime(tc.et2datetime(self.stoptime),
                                                   '%Y-%m-%d %H:%M:%S'),
                       ' End time of observation')
        hdr.set('SINEXP',
                       self.singleexp,
                       'Exposure time for one spectrum, if constant')
        hdr.set('TOTALEXP',
                       self.totalexp,
                       'Total exposure time of the observation')
        hdr.set('RECORDS',
                       self.fullnumrec,
                       'Number of file records included in this image')
        # determine which hemisphere was observed
        north = np.nansum(self.proj_sum[:,-int(60*self.PROJ_RES):]>0)
        south = np.nansum(self.proj_sum[:,:int(60*self.PROJ_RES)]>0)
        if north>0 and south>0:
            print('Warning: Pixels in both hemispheres')
        if north > south:
            hem = 'North'
        else:
            hem = 'South'            
        hdr.set('HEMSPH',
                       hem,
                       'Observed hemisphere')
        
        if style=='INT':
            hdr.set('INTEGRAT',
                    self.intmethod,
                    'Integration method')
            hdr.set('AXIS1',
                    '-90..90',
                    'Latitude axis definition')
            hdr.set('AXIS2',
                    '0..360',
                    'Longitude axis definition')
           
        elif style=='SPEC':
            hdr.set('AXIS1',
                    'TABLE, FREQ (A)',
                    'Spectral axis definition')
            hdr.set('AXIS2',
                    '-90..90',
                    'Latitude axis definition')
            hdr.set('AXIS3',
                    '0..360',
                    'Longitude axis definition')

    def saveFits(self, fname=None, saveall=True, plotangles=True):
        print('Saving fits file')
        # gotta reset spice error handling because sincpt errors don't get 
        # cleaned out properly for some reason
        spice.reset()
        proj = self.proj_sum/self.proj_num
        if not np.any(proj):
            print('Empty projection')
            return
        # have a look how much data there actually is
        # we want at least a quarter of one hemisphere up to colatitude 30 deg
        # otherwise discard plot
        if not saveall:
            minpx = np.sum(np.ones_like(self.proj_sum))/6/80
            projpx = np.sum(np.ones_like(self.proj_sum))-np.sum(np.isnan(proj))
            if projpx < minpx:
                print('Not enough coverage, no file saved')
                return
            # if no data in polar caps with colatitude < 20 deg, discard
            northcap = np.nansum(self.proj_sum[:,-int(40*self.PROJ_RES):]>0)
            southcap = np.nansum(self.proj_sum[:,:int(40*self.PROJ_RES)]>0)
            if not northcap and not southcap:
                print('Not enough coverage, no file saved')
                return
        
        # encapsulate data
        hdu = fits.PrimaryHDU(proj)
        hdu_angles = fits.ImageHDU(self.proj_minangle)
        self.setHeader(hdu.header, style='INT')
        # set filename and path, save fits
        if fname==None:
            fullpath = '%s/%s' % (self.savepath,
                                  datetime.strftime(tc.et2datetime(self.starttime),
                                                       '%Y_%jT%H_%M_%S'))
        else:
            fullpath = '%s/%s' % (self.savepath,
                                  fname)
            
        print(fullpath)
        hdul = fits.HDUList([hdu, hdu_angles])
        hdul.writeto('%s.fits' % fullpath, overwrite=True)
#        hdu.writeto('%s.fits' % fullpath, overwrite=True)
        
        # plotting right hemisphere
        print('Plotting projection')
        north = np.nansum(self.proj_sum[:,-int(60*self.PROJ_RES):]>0)
        south = np.nansum(self.proj_sum[:,:int(60*self.PROJ_RES)]>0)
        if north > south:
            hem = 'North'
        else:
            hem = 'South'
        # make quick plot
        if hem == 'South':
            plotproj = proj[:,:int(60*self.PROJ_RES)]
            angleproj = self.proj_minangle[:,:int(60*self.PROJ_RES)]
        else:
            plotproj = proj[:,-int(60*self.PROJ_RES):]
            plotproj = plotproj[:,::-1]
            angleproj = self.proj_minangle[:,-int(60*self.PROJ_RES):]
            angleproj = angleproj[:,::-1]
        title = '%s\n%s\n%s, %s s' % (
                datetime.strftime(tc.et2datetime(self.starttime), '%Y-%j %H:%M:%S'),
                datetime.strftime(tc.et2datetime(self.starttime), '%Y-%m-%d %H:%M:%S'),
                                       hem, self.totalexp)
        self.simpleplot(plotproj, fullpath, title, style='LT', KR_MAX=20, scale='log')
        if plotangles:
            self.simpleplot(angleproj, '{}_angle'.format(fullpath), title,
                            style='LT', KR_MIN=0,KR_MAX=90, scale='lin',
                            nlev=10, cbarlabel='Cassini elevation over horizon (deg)')
        
        # write spectral distributions to file
        if self.savespecs:
            print('Saving spectral fits file')
            proj_spec = self.proj_spec_sum/self.proj_spec_num
            hdu_spec = fits.PrimaryHDU(proj_spec)
            self.setHeader(hdu_spec.header, style='SPEC')
            col1 = fits.Column(name='Index', format='E',
                               array=np.arange(len(self.wave)), ascii=True)
            col2 = fits.Column(name='Freq', format='E',
                               array=self.wave, ascii=True)
            hdu_tbl = fits.TableHDU.from_columns([col1, col2])
            hdul = fits.HDUList([hdu_spec, hdu_tbl])
            fullpath = '%s/%s_spec' % (self.savepath,
                              datetime.strftime(tc.et2datetime(self.starttime),
                                                   '%Y_%jT%H_%M_%S'))
            print(fullpath)
            hdul.writeto('%s.fits' % fullpath, overwrite=True)

    def previewFiles(self, filelist, LASP=False):
        numfiles = len(filelist)
        for iii in range(numfiles):
            current = filelist[iii]
            print(current)
            try:
                self.getCalibIntegData(current, LASP=LASP)
            except DataError as e:
                print(e)
                continue
            except Exception as e:
                print(e)
                print(self.metadata)
                continue
            name = current.split('/')[-1]
            name = current.split('\\')[-1]
            name = name.strip('.dat')
            try:
                plt.pcolormesh(self.int_data)
            except:
                pass
            else:
                plt.savefig('%s/%s.png' % (self.savepath, name), bbox_inches='tight', dpi=500)
                plt.close()   
        
    def stitchFiles(self, filelist, LASP=False, clean=True, sens=1):
        self.resetProjection()
        numfiles = len(filelist)
        for iii in range(numfiles):
            current = filelist[iii]
            self.getCalibIntegData(current, LASP=LASP)
            self.getFovVectors()
            if clean:
                minlist, maxlist = self.getSplitLimits(sens=sens)
                print(minlist)
                print(maxlist)
                for jjj in range(len(minlist)):
                    currentmin = int(minlist[jjj])
                    currentmax = int(maxlist[jjj])
                    self.projectCurrent(recmin=currentmin, recmax=currentmax)
            else:                    
                self.projectCurrent()
            # first file determines start time
            if iii == 0:
                self.starttime = self.ettimes[0]
                self.singleexp = self.metadata['INTEGRATION_DURATION']
            # last file determines stop time
            if iii == numfiles-1:
                self.stoptime = self.ettimes[-1]
            # check whether integration time is always the same
            if self.metadata['INTEGRATION_DURATION'] != self.singleexp:
                self.singleexp = 'VARIABLE'
            # total number of file records
            if clean:
                self.fullnumrec += np.sum(maxlist-minlist)
            else:
                self.fullnumrec += self.metadata['FILE_RECORDS']
        # calculate total exposure
        if type(self.singleexp) != float and type(self.singleexp) != int:
            self.totalexp = int(self.stoptime-self.starttime)
        else:
            self.totalexp = int(self.fullnumrec*self.singleexp)
        if np.sum(self.proj_num):
            self.saveFits()
            
    def getSplitLimits(self, sens=1):
        # see how many records we have
        numrecs = int(self.metadata['FILE_RECORDS'])
            
        # determine limits automatically using pointing of boresight vector
        boresight = np.array([0,0,1])
        # find boresight vector in KSM
        boresight_KSM = np.zeros((0,3))
        for iii in range(len(self.ettimes)):
            try:
                tmp = np.matmul(spice.pxform('CASSINI_UVIS_FUV',
                                             'CASSINI_KSM',
                                             self.ettimes[iii]),
                                boresight)
            except:
                
                tmp = np.array([np.nan,np.nan,np.nan])
            boresight_KSM = np.append(boresight_KSM, [tmp], axis=0)
        # find angular difference between boresight at each timestep
        self.angle_diff = np.array([np.arccos(np.dot(boresight_KSM[iii],
                                                boresight_KSM[iii+1]))
            for iii in range(len(boresight_KSM)-1)])
        # find gradient of the difference
        diff_grad = np.gradient(self.angle_diff)
        # find where the gradient is high, aka the boresight angle change
        # is accelerated compared to the median
        ind = np.where(np.abs(diff_grad)>sens*np.nanmean(np.abs(diff_grad)))[0]
        ind = np.insert(ind,0,0)
        ind = np.insert(ind,len(ind),numrecs-1)
        # find first period with reasonably long constant change in angle
        # and calculate average rotation rate in this period
        #tmp = np.where(np.diff(ind)>np.nanmean(np.diff(ind))/2)[0]
        tmp = np.where(np.diff(ind)>np.nanmean(np.diff(ind)[np.diff(ind)>1])/2)[0]
        avg_diff = np.nanmean(np.abs(self.angle_diff[ind[tmp[0]]:ind[tmp[0]+1]]))
        # go through list of indices with increased angular change
        minlist = np.array([])
        maxlist = np.array([])
        for iii in range(len(ind)-1):
            # discard short periods (aka scans while the s/c was rotating
            # back to start position))
            if (ind[iii+1] - ind[iii]) < 5:
                continue
            # discard scans with higher and lower rotation rates
            if np.nanmean(np.abs(self.angle_diff[ind[iii]:ind[iii+1]])) > 1.25*avg_diff*sens:
                print('a')
                continue
            if np.nanmean(np.abs(self.angle_diff[ind[iii]:ind[iii+1]])) < 0.8*avg_diff/sens:
                print('b')
                continue
            minlist = np.append(minlist, ind[iii])
            maxlist = np.append(maxlist, ind[iii+1]+1)
        return minlist, maxlist

        
    def splitFile(self, file, LASP=False, NMAX=1e3, sens=1, only='all'):
        self.resetProjection()
        try:
            self.getCalibIntegData(file, LASP=LASP)
            self.getFovVectors()
        except Exception as e:
            print(e)
            return        
        minlist, maxlist = self.getSplitLimits(sens=sens)
        print(minlist)
        print(maxlist)
        if not np.size(minlist):
            return
        if NMAX < len(minlist):
            minlist = minlist[:NMAX]
            maxlist = maxlist[:NMAX]
        indrange = range(len(minlist)) if only=='all' else [only]
        for iii in indrange:
            try:
                # project selected records
                self.resetProjection()
                currentmin = int(minlist[iii])
                currentmax = int(maxlist[iii])
                self.projectCurrent(recmin=currentmin, recmax=currentmax)
                # get some parameters
                self.starttime = self.ettimes[currentmin]
                self.stoptime = self.ettimes[currentmax]
                self.singleexp = self.metadata['INTEGRATION_DURATION']
                self.fullnumrec = currentmax-currentmin
                self.totalexp = int(self.ettimes[currentmax]-self.ettimes[currentmin])
                self.saveFits()
            except Exception as e:
                print(e)
                
    def projectFromTimes(self, filelist, etstart, etstop, LASP=False, fname=None):
        self.resetProjection()
        for file in filelist:
            try:
                self.getCalibIntegData(file, LASP=LASP)
                self.getFovVectors()
                # get record limits to clean out spacecraft slews
                minlist, maxlist = self.getSplitLimits()
            except Exception as e:
                print(e)
                continue
            # all records within time limits
            tmp = np.where((self.ettimes>etstart) & (self.ettimes<etstop))[0]
            valid = np.array([True if np.any((iii>=minlist)&(iii<=maxlist)) else False
                              for iii in tmp])
            reclist = tmp[np.where(valid)]
            self.reclist = reclist
            print(reclist)
            if len(reclist):
                try:
                    self.projectCurrent(reclist=reclist)
                    # set parameters
                    if self.starttime == None:
                        self.starttime = np.min(self.ettimes[reclist])
                        self.stoptime = np.max(self.ettimes[reclist])
                        self.singleexp = self.metadata['INTEGRATION_DURATION']
                        self.fullnumrec = len(reclist)
                        self.totalexp = int(self.metadata['INTEGRATION_DURATION']*len(reclist))
                    else:
                        if self.starttime > np.min(self.ettimes[reclist]):
                            self.starttime = np.min(self.ettimes[reclist])
                        if self.stoptime < np.max(self.ettimes[reclist]):
                            self.stoptime = np.max(self.ettimes[reclist])
                        if self.singleexp != self.metadata['INTEGRATION_DURATION']:
                            self.singleexp = 'VARIABLE'
                        self.fullnumrec += len(reclist)
                        self.totalexp += int(self.metadata['INTEGRATION_DURATION']*len(reclist))
                except Exception as e:
                    print(e)
                    continue
        try:
            self.saveFits(saveall=True, fname=fname, plotangles=False)
        except:
            pass
        
    def generalFigureSetup(self, ax, style='LT'):
        # general figure setup
        ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
        ax.set_xticks(ticks)
        if style == 'LT':
            ax.set_theta_zero_location("N")
            ticklabels = ['00','06','12','18']
            for iii in range(len(ticklabels)):
                txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                              ha='center',va='center')
                txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
            ax.set_xticklabels([])
        elif style == 'PPO':
            ax.set_xticklabels(['0','90','180','270'])
            ax.set_theta_zero_location("S")
            ax.set_theta_direction(-1)
        ax.set_yticks([10,20,30])
        ax.set_yticklabels([])
        ax.set_rmax(30)
        ax.set_rmin(0)
        ax.grid('on', color='0.8', linewidth=1)
        
    def plotImage(self, ax, image, cax, scale='log', KR_MIN=0.3, KR_MAX=20, nlev=0, cbarlabel='Intensity (kR)'):
        ax.set_facecolor('gray')
        if scale=='log':
            image[np.where(image<=0)] = 0.00001
        # pcolormesh
        if not nlev:
            theta = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1,endpoint=True)
            r = np.linspace(0,30,num=np.shape(image)[1]+1,endpoint=True)
            quad = ax.pcolormesh(theta, r, image.T, cmap=cmap_UV)
            quad.set_clim(0, KR_MAX)
            if scale=='log':
                quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
            cbar = plt.colorbar(quad, pad=0.1, cax=cax, extend='both')
            cbar.set_label(cbarlabel, rotation=270, labelpad=20)
            if scale=='log':
                ticklist = np.append(np.append(np.linspace(0.1,0.9,num=9),
                                                   np.linspace(1,9,num=9)),
                                         np.linspace(10,90,num=9))
            else:
                ticklist = np.linspace(0, KR_MAX, num=11, endpoint=True)
            cbar.set_ticks(ticklist)
        # contourf
        else:
            theta = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1,endpoint=True)
            r = np.linspace(0,30,num=np.shape(image)[1]+1,endpoint=True)
            theta = theta[:-1]+np.diff(theta)/2
            r = r[:-1]+np.diff(r)/2
            if scale=='log':
                levels = np.logspace(np.log10(KR_MIN),np.log10(KR_MAX), num=nlev)
            else:
                levels = np.linspace(KR_MIN,KR_MAX,num=nlev)
            quad = ax.contourf(theta,r,image.T,levels,cmap=cmap_UV,extend='both')
            quad.cmap.set_under('k')
            quad.cmap.set_over('w')
            cbar = plt.colorbar(quad, pad=0.1, cax=cax, extend='both')
            cbar.set_label(cbarlabel, rotation=270, labelpad=20)
            if scale=='log':
                cbar.set_ticks(np.append(np.append(np.linspace(0.1,0.9,num=9),
                                                   np.linspace(1,9,num=9)),
                                         np.linspace(10,90,num=9)))
            else:
                cbar.set_ticks(np.linspace(0, KR_MAX, num=nlev, endpoint=True))
            
    def simpleplot(self, redImage, savefilename, title, cbarlabel=None,
                   style='LT', KR_MIN=0.3, KR_MAX=20, scale='log', nlev=0):
        # make figure
        fig = plt.figure()
        fig.set_size_inches(7,7)
        gs = gridspec.GridSpec(1, 2, width_ratios=(1, 0.05))
        ax = plt.subplot(gs[0,0], projection='polar')
                
        if cbarlabel:
            self.plotImage(ax, redImage, plt.subplot(gs[0,1]), scale=scale, KR_MIN=KR_MIN, KR_MAX=KR_MAX, nlev=nlev, cbarlabel=cbarlabel)
        else:
            self.plotImage(ax, redImage, plt.subplot(gs[0,1]), scale=scale, KR_MIN=KR_MIN, KR_MAX=KR_MAX, nlev=nlev)
        self.generalFigureSetup(ax, style=style)
        
        ax.set_title(title, y=1.1)
        # save and close
        plt.savefig('{}.png'.format(savefilename), bbox_inches='tight', dpi=500)
#        plt.show()
        plt.close()

