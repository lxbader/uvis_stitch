import path_register

pr = path_register.PathRegister()
datapath = pr.datapath

import stitch_calibrate_project

import glob
import numpy as np
import os
import sys

us = stitch_calibrate_project.UVISstitcher()

mainsavepath = '{}/UVIS/PDS_UVIS/PROJECTIONS_RES1_ISS_1650_1800'.format(datapath)
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)
uvispath = '{}/UVIS/PDS_UVIS'.format(datapath)

actfile = '{}/active.txt'.format(mainsavepath)

# list of available releases
rlslist = glob.glob('%s/*' % uvispath)
rlslist = [rlslist[iii] for iii in range(len(rlslist))
                if ('.' not in rlslist[iii] and 'COUVIS' in rlslist[iii])]
rlsnumbers = np.array([int(rlslist[iii].split('COUVIS_')[-1]) for iii in range(len(rlslist))])

# some status saving to run it on several kernels in parallel
def saveStatus(status):
    np.savetxt(actfile, status, fmt='%d', delimiter=',')
    return

def updateStatus():
    # import current status
    # 0 = not started, 1 = in progress, 2 = finished
    tmp = glob.glob(actfile)
    if not tmp:
        status = np.append([rlsnumbers], [np.zeros_like(rlsnumbers)], axis=0).T
    else:
        status = np.genfromtxt(actfile, delimiter = ',', dtype=np.int)
    saveStatus(status)
    return status

# defining different groups of releases of approximately the same size to run
# concurrently in several consoles
full = [19,21,22,23,24,25,26,39,41,42,43,44,45,46,47,48,49,54,55,56,57,58,59,60]
#group1 = [19,21,22,23,24,25,26,41,42,43,44,45]
#group2 = [47]
#group3 = [48,49,54,55,56,57]
#group4 = [46,58,59,60]

#status = updateStatus()

#while np.any(status[:,1] == 0):
#    try:
#        argcurrent = np.where(status[:,1] == 0)[0][0]
#        release = status[argcurrent,0]
#    except:
#        break
#    else:
#        status[argcurrent,1] = 1
#        saveStatus(status)

for release in full:    
    if release<90:
        tmp = np.where(rlsnumbers == release)[0]
        if not np.size(tmp):
            continue
        rlstitle = rlslist[tmp[0]].split('\\')[-1]
        info = np.genfromtxt('%s/%s_info.txt' % (uvispath,rlstitle), delimiter=',', dtype='U')
        allLASP = True if len(glob.glob(os.path.join(uvispath,rlstitle, 'LASP.txt'))) else False
        us.setSavePath('%s/%s' % (mainsavepath,rlstitle))
    else:
        info = np.genfromtxt('%s/SELEC_DAWN_STORM.txt' % (uvispath), delimiter=',', dtype='U')
        us.setSavePath(mainsavepath)
        allLASP = 'indef'
        allLASP = True
    
    us.setResolution(1)
    us.saveSpecs(False)
    us.intmethod = 'REFL_SUNLIGHT'
    
    currentset = []
    if len(np.shape(info)) != 2:
        continue
    
#    range1 = range(0,56)
#    range2 = range(56,np.shape(info)[0])
    
    for fctr in range(np.shape(info)[0]):
        if info[fctr,1] == 'SKIP':
            continue
        file = '%s/%s' % (uvispath, info[fctr,0])
        
        if allLASP == 'indef':
            LASP = True if 'COUVIS_0060' in file else False
        else:
            LASP = allLASP
        
        if 'AURORA' in info[fctr,1]:
            continue
            
        if 'COMBINE_START' in info[fctr,1]:
            currentset = [file]
            continue
        elif 'CONT' in info[fctr,1]:
            currentset.append(file)
            continue
        elif 'COMBINE_STOP' in info[fctr,1]:
            currentset.append(file)
            try:
                print(currentset)
                if 'CLEANTRUE' in info[fctr,1]:
                    if 'SENS' in info[fctr,1]:
                        us.stitchFiles(currentset, LASP=LASP, clean=True, sens=int(info[fctr,1][-1]))
                    else:
                        us.stitchFiles(currentset, LASP=LASP, clean=True)
                else:
                    us.stitchFiles(currentset, LASP=LASP, clean=False)
            except Exception as e:
                print(e)
                pass
        elif 'SPLIT_AUTO' in info[fctr,1]:
            try:
                if 'AURORA' in info[fctr,1]:
                    us.splitFile(file, NMAX=2, LASP=LASP)
                if 'SENS' in info[fctr,1]:
                    us.splitFile(file, LASP=LASP, sens=int(info[fctr,1][-1]))
                if 'ONLY' in info[fctr,1]:
                    us.splitFile(file, LASP=LASP, only=int(info[fctr,1].split('ONLY')[-1]))
                else:
                    us.splitFile(file, LASP=LASP)
            except Exception as e:
                print(e)
                pass
        elif 'SINGLE' in info[fctr,1]:
            try:
                if 'NOCLEAN' in info[fctr,1]:
                    us.stitchFiles([file], LASP=LASP, clean=False)
                else:
                    if 'SENS' in info[fctr,1]:
                        us.stitchFiles([file], LASP=LASP, sens=int(info[fctr,1][-1]))
                    else:
                        us.stitchFiles([file], LASP=LASP)
            except Exception as e:
                print(e)
                pass
        else:
            print('Unrecognized flag: %s' % info[fctr,1])
            
#    status = updateStatus()
#    status[argcurrent,1] = 2
#    saveStatus(status)
